# Copyright (c) 2022 Kyle Howen
# All Rights Reserved.

import os, re
from setuptools import setup


def read(*rnames):
    return open(os.path.join(os.path.dirname(__file__), *rnames)).read()

VERSION = re.findall(r"\d+", read("subinitial", "equipment", "version.py"))


setup(
    name='subinitial.equipment',
    version='{}.{}.{}'.format(*VERSION),
    description='Subinitial provided 3rd Party Test Equipment Drivers',
    packages=['subinitial.equipment', 'subinitial'],
    include_package_data=True,
    license='UNLICENSED',
    url='https://bitbucket.org/subinitial/subinitial-equipment.git',
    author='Kyle Howen',
    author_email='kyle.howen@subinitial.com',
    long_description=read("README.md"),
    keywords="subinitial",
    classifiers = [
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: Other/Proprietary License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
    ],
    extras_require={
        'daq':['pyvisa-py==0.6.3'],
        'can':[
            'cantools==38.0.1',
            'python-can==4.1.0',
            'canlib==1.22.565'
        ]
    }
)
