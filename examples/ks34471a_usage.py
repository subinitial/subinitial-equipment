
from subinitial.equipment.ks34461a import Keysight34461A


def main():
    dmm = Keysight34461A("172.17.32.31", autoconnect=False)

    dmm.connect()
    print(dmm.idn)

    v = dmm.measure_voltage()
    print("voltage", v)

    i = dmm.measure_current()
    print("current (auto range, 3A terminals)", i)

    i10 = dmm.measure_current(range=dmm.IRANGE_10A)
    print("current (10A terminals)", i10)

    r = dmm.measure_resistance()
    print("resistance", r)


if __name__ == "__main__":
    main()
