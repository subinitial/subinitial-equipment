# TDK-Lambda Z+ Usage Example

from subinitial.equipment.tdkzplus import TdkZPlus


def main():
    comport = listports()
    test(comport)


def listports():
    # optionally discover existing connections
    ports = TdkZPlus.list_serial_connections(_print=True)
    device_idx = input("Select device by #")
    port = ports[int(device_idx)]
    print("Selected Device", port.device, port.hwid)
    return port.device


def test(comport):
    zplus = TdkZPlus(com_port=comport)

    zplus.reset()  # reset to default settings

    zplus.set_voltage(3.5)
    zplus.set_voltage(1, 3.5)  # this is identical to zplus.set_voltage(3.5) as the channel parameter is optional in this driver
    zplus.set_current(0.5)
    zplus.output_enable()

    print(zplus.get_voltage_command(), zplus.get_current_command(), zplus.get_output())
    print(zplus.get_voltage_measured(), zplus.get_current_measured(), zplus.get_power_measured())

    # change multiple parameters at once and wait for outputs to settle
    zplus.output(voltage=5, current=0.25, enable=True)

    zplus.output_disable(1)  # you can specify the channel but it wont be used. This way you can reuse SorensonXPF code with TdkZPlus code


if __name__ == "__main__":
    main()
