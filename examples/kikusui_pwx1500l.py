
from subinitial.equipment.pwx1500l import PWX1500L


def main():
    pwx = PWX1500L("172.17.31.5")
    # pwx.set_voltage(3.3)
    # pwx.set_current(0.1)
    #
    # pwx.set_overvoltage_protection(5.0)
    # pwx.set_overcurrent_protection(2.0)
    print("OVP,OCP", pwx.get_overvoltage_protection(), pwx.get_overcurrent_protection())
    print(pwx.get_output(), pwx.get_voltage_command(), pwx.get_current_command())
    # pwx.output_enable()
    print(pwx.get_voltage_measured(), pwx.get_current_measured())
    pass


if __name__ == "__main__":
    main()
