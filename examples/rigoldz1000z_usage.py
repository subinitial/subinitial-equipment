
from subinitial.equipment.rigoldg1000z import RigolDG1000Z


def main():
    siggen = RigolDG1000Z("172.17.32.30", autoconnect=False)

    siggen.connect()
    print(siggen.idn)

    siggen.output(True)
    print(siggen.get_output(), siggen.get_impedance())

    siggen.impedance(siggen.IMPEDANCE_50OHM)
    print(siggen.get_impedance())

    vsignal = 3.3
    siggen.waveform(1, shape=siggen.SHAPE_SQUARE, frequency=10e6, vamplitude=vsignal, voffset=vsignal/2)
    print(siggen.get_waveform(1))

    input(">")
    siggen.output(False)


if __name__ == "__main__":
    main()
