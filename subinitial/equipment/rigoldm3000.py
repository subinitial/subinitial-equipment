# -*- coding: UTF-8 -*-
# Subinitial Test Automation Framework rigoldm3000 series interface
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import logging

logger = logging.getLogger(__name__)

class RigolDM3000:
    vid = 0x1AB1
    pid = 0x0C94
    _mode_usb = "USB"
    _mode_dc = "DISCONNECTED"
    _mode_tcpip = "TCPIP"
    _retrycount = 3

    def __init__(self, serial=None):
        self.serial = serial
        self.mode = RigolDM3000._mode_dc
        self.idn = None
        self.ipaddr = None
        self.device = None
        """:type : usbtmc.Instrument"""

    def is_connected(self):
        return self.idn is not None

    def connect_usb(self):
        self.mode = RigolDM3000._mode_usb
        retries = 0
        import usbtmc
        while True:
            try:
                if self.serial is None:
                    self.device = usbtmc.Instrument(RigolDM3000.vid, RigolDM3000.pid)
                else:
                    self.device = usbtmc.Instrument(RigolDM3000.vid, RigolDM3000.pid, iSerial=self.serial)
                break
            except Exception as ex:
                retries += 1
                logger.info("connect failed on try {} with exception: {}".format(retries, ex))
                if retries > RigolDM3000._retrycount:
                    raise ex
        self.device.write(" ")
        self.idn = self._get_idn()

    def connect_tcpip(self, ipaddress):
        self.mode = RigolDM3000._mode_tcpip
        self.ipaddr = ipaddress
        import vxi11
        retries = 0
        while True:
            try:
                self.device = vxi11.Instrument(self.ipaddr)
                """ THE FOLLOWING TIMEOUT VALUES ARE CRITICAL, THE DMM WILL REPORT THAT IT IS LOCKED IF THESE AREN'T SET
                    The values come from packet sniffing Rigol's Ultra Sigma software while connecting to a DM3068"""
                self.device.lock_timeout = 0
                self.device.timeout = 2000
                self.idn = self._get_idn()
                break
            except Exception as ex:
                retries += 1
                logger.info("connect failed on try {} with exception: {}".format(retries, ex))
                if retries > RigolDM3000._retrycount:
                    raise ex

    def _get_idn(self):
        retries = 0
        while True:
            try:
                return self.device.ask("*IDN?")
            except Exception as ex:
                retries += 1
                logger.info("communication for *IDN? failed on try {} with exception: {}".format(retries, ex))
                if retries > RigolDM3000._retrycount:
                    raise ex

    def close(self):
        self.idn = None
        import usb.util
        if self.mode == RigolDM3000._mode_usb and self.device is not None:
            self.device.reset()  # TODO: this method does nothing, find a way to reset the SCPI command interface
            usb.util.release_interface(self.device.device, self.device.iface)

    def __enter__(self):
        return self

    def __exit__(self):
        self.close()

    # DEVICE SPECIFIC CODE BELOW

    TRIGGER_SOURCE_AUTO = "AUTO"
    TRIGGER_SOURCE_SINGLE = "SINGLE"

    def set_trigger_source(self, source="AUTO", trigger_sample_count=1):
        self.device.write(":TRIGger:SOURce " + source)
        self.device.write(":TRIGger:SINGle {}".format(trigger_sample_count))

    def get_trigger_source(self):
        return self.device.ask(":TRIGger:SOURce?")

    def force_trigger(self):
        self.device.write(":TRIGger:SINGle:TRIGger")

    def get_voltage(self):
        """:return: float """
        response = self.device.ask(":MEAS:VOLT:DC?")
        return float(response)

    def get_current(self):
        """:return: float """
        response = self.device.ask(":MEAS:CURR:DC?")
        return float(response)

    def get_resistance(self):
        """:return: float """
        response = self.device.ask(":MEAS:RES?")
        return float(response)

    IRANGE_200UA = 0
    IRANGE_2MA = 1
    IRANGE_20MA = 2
    IRANGE_200MA = 3
    IRANGE_2A = 4
    IRANGE_10A = 5

    def set_current_range(self, range_):
        """:return: float """
        self.device.write(":MEAS:CURR:DC {}".format(range_))

    def get_acvoltage(self):
        """:return: float """
        response = self.device.ask(":MEAS:VOLT:AC?")
        return float(response)

    RANGE_AUTO = -1
    RANGE_200MV = 0
    RANGE_2V = 1
    RANGE_20V = 2
    RANGE_200V = 3
    RANGE_1000V = 4

    dm306ranges = { RANGE_200MV: 0.2, RANGE_2V: 2, RANGE_20V: 20,
                    RANGE_200V: 200, RANGE_1000V: 1000 }

    def get_range(self):
        """:return: int """
        response = self.device.ask(":MEAS:VOLT:DC:RANG?")
        return int(response)

    def set_mode(self, auto_not_manual):
        """:param auto_not_manual bool:"""
        if auto_not_manual:
            self.device.write(":MEAS AUTO")
        else:
            self.device.write(":MEAS MANU")

    def set_range(self, range_):
        """:param range_ int:"""
        if range_ == RigolDM3000.RANGE_AUTO:
            self.set_mode(True)
        elif RigolDM3000.RANGE_200MV <= range_ <= RigolDM3000.RANGE_1000V:
            self.device.write(":MEAS:VOLT:DC {}".format(range_))

    def set_range_by_voltage(self, volts, over_range=0.1):
        volts = abs(volts)
        for range in self.dm306ranges:
            maxv = self.dm306ranges[range]
            if volts < maxv * (1 + over_range):
                self.set_range(range)
                return
        self.set_range(self.RANGE_1000V)  # Default to max range
