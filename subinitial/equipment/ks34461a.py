# -*- coding: UTF-8 -*-
# Subinitial Test Automation Framework rigoldm3000 series interface
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

# More info at https://rfmw.em.keysight.com/bihelpfiles/Truevolt/WebHelp-Mobile/US/Advanced/Default.htm

import socket
import logging
from enum import Enum

logger = logging.getLogger(__name__)


class Keysight34461A:
    _retrycount = 3

    def __init__(self, host=None, autoconnect=True):
        """Creates an instance to remotely control the test equipment.

        :param host: tcp/ip host as ip-address or Netbios hostname
        :type host: string
        :param autoconnect: if True the device will be connected to at instantiation
        :type autoconnect: bool
        """
        self._host = host
        self._port = 5025
        self._timeout = 5
        self._buffer_size = 2048
        self._socket = None
        self.idn = None
        self.connected = False
        if autoconnect and host:
            self.connect(host=self._host)

    def initialize(self):
        '''Put the equipment in a known and ready to work state'''
        self.reset(True)

    def connect(self, host=None):
        """Connect to the device via LAN

        :param host: tcp/ip host as ip-address or Netbios hostname
        :type host: string
        :returns: True if connection was made, False if already connected to host
        :rtype: bool
        """
        if host is not None and self._host != host:
            self.close()
            self._host = host
        elif self.connected:
            return False

        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.settimeout(self._timeout)
        self._socket.connect((self._host, self._port))
        self.idn = self._ask("*IDN?")
        self.connected = True
        # if device_id != self.ID:
        #     raise Exception("Invalid Device ID? response: {}, expected {}".format(device_id, self.ID))
        return True

    def set_timeout(self, timeout_s=5):
        self._timeout = timeout_s
        if self._socket is not None:
            self._socket.settimeout(self._timeout)

    def _write(self, msgstr):
        """Send command to the device but don't wait for a response

        :param msgstr: message string
        :type msgstr: str
        """
        msgstr = msgstr.strip() + "\r\n"
        self._socket.send(msgstr.encode())

    def _ask(self, msgstr):
        """Send command to the device and read response

        :param msgstr: message string
        :type msgstr: str
        :return: GPIB response string
        :rtype: str
        """
        self._write(msgstr)
        response = self._socket.recv(self._buffer_size).decode()
        return response.strip()

    def close(self):
        """Close the device connection"""
        if self.connected:
            if self._socket:
                self._socket.close()
            self._socket = None
            self.connected = False

    def __enter__(self):
        return self

    def __exit__(self):
        self.close()

    def is_operation_complete(self):
        """Get operation status of the device

        ..note: this will most likely always return True as the device will not
                respond to it until it is finished completing all of its queued operations
        :return: True if all operations are complete and device is ready for next command
        :rtype: bool
        """
        opc = self._ask("*OPC?")
        return opc != "0"

    def reset(self, wait_for_response=False):
        """Reset the device to its default state"""
        self._write("*RST")
        if wait_for_response:
            return self.is_operation_complete()
        return 0

    # DEVICE SPECIFIC CODE BELOW

    TRIGGER_SOURCE_AUTO = "AUTO"
    TRIGGER_SOURCE_SINGLE = "SINGLE"

    def set_trigger_source(self, source="AUTO", trigger_sample_count=1):
        self._write(":TRIGger:SOURce " + source)
        self._write(":TRIGger:SINGle {}".format(trigger_sample_count))

    def get_trigger_source(self):
        return self._ask(":TRIGger:SOURce?")

    def force_trigger(self):
        self._write(":TRIGger:SINGle:TRIGger")

    RESOLUTION_10PLC = 10
    RESOLUTION_1PLC = 1
    RESOLUTION_0PLC1 = 0.1

    AUTO = "AUTO"
    DEFAULT = "DEFAULT"
    VRANGE_100MV = 0.1
    VRANGE_1V = 1
    VRANGE_10V = 10
    VRANGE_100V = 100
    VRANGE_1000V = 1000

    MODE_DC = "DC"
    MODE_AC = "AC"

    MEASURE_RESOLUTION_NOAUTOZERO = {
        0.001: 1e-8,
        0.010: 1e-7,
        0.100: 1e-6,
            1: 1e-5,
           10: 1e-4,
          100: 1e-3,
          1e3: 1e-2,
          1e4: 1e-1,
          1e5: 1,
          1e6: 10,
          1e7: 100,
          1e8: 1e3,
       "AUTO": "AUTO"
    }

    def measure_voltage(self, range: str|int|float=AUTO, mode=MODE_DC, resolution=DEFAULT):
        """:return: float """
        if resolution is self.MEASURE_RESOLUTION_NOAUTOZERO:
            resolution = self.MEASURE_RESOLUTION_NOAUTOZERO[range]
        response = self._ask(f":MEAS:VOLT:{mode}? {range},{resolution}")
        return float(response)

    IRANGE_10A = 10
    IRANGE_3A = 3
    IRANGE_1A = 1
    IRANGE_100mA = 0.1
    IRANGE_10mA = 0.01
    IRANGE_1mA = 0.001
    IRANGE_100uA = 100e-6

    def measure_current(self, range: str|int|float=AUTO, mode=MODE_DC, resolution=DEFAULT):
        """:return: float """
        if resolution is self.MEASURE_RESOLUTION_NOAUTOZERO:
            resolution = self.MEASURE_RESOLUTION_NOAUTOZERO[range]
        response = self._ask(f":MEAS:CURR:{mode}? {range},{resolution}")
        return float(response)

    RRANGE_100Ohm = 100
    RRANGE_1kOhm = 1e3
    RRANGE_10kOhm = 10e3
    RRANGE_100kOhm = 100e3
    RRANGE_1MOhm = 1e6
    RRANGE_10MOhm = 10e6
    RRANGE_100MOhm = 100e6
    RRANGE_1GOhm = 1e9

    def measure_resistance(self, range: int|str|float=AUTO, resolution=DEFAULT):
        """:return: float """
        if resolution is self.MEASURE_RESOLUTION_NOAUTOZERO:
            resolution = self.MEASURE_RESOLUTION_NOAUTOZERO[range]
        response = self._ask(f":MEAS:RES? {range},{resolution}")
        val = float(response)
        if val > 1e20:
            return 9.99e10
        return val
    
    def measure_diode(self):
        """:return: float """
        response = self._ask(f":MEAS:DIODe?")
        return float(response)


    AUTOZERO_OFF = "OFF"
    AUTOZERO_ON = "ON"  # Default
    AUTOZERO_ONCE = "ONCE"

    def autozero_set(self, setting):
        response = self._write(f"SENSe:VOLTage:DC:ZERO:AUTO {setting}")
    
    def autozero_get(self) -> bool:
        return self._ask(":SENSe:VOLTage:DC:ZERO:AUTO?") == "1"

    NPLC_20m = 0.02
    NPLC_200m = 0.002
    NPLC_1 = 1
    NPLC_10 = 10   # Default
    NPLC_100 = 100

    def nplc_set(self, nplc):
        """
        @param nplc: 0.02, 0.2, 1, 10, 100
        """
        response = self._write(f":SENSe:VOLTage:DC:NPLC {nplc}")
    
    def nplc_get(self) -> float:
        resp = self._ask(":SENSe:VOLTage:DC:NPLC?")
        return float(resp)
