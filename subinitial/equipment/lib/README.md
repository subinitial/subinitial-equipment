#
LICENSE AND COPYRIGHT:
	-Each folder in this directory represents a 3rd party python package that has been included with subinitial.test for automatic fall-back usage when these packages are not found in your existing python library. All are optional or situationaly dependant. Included are weblinks for each package, please refer to these websites for copyrights, licensing, and author information.

serial - https://pypi.python.org/pypi/pyserial/2.7
usb - https://pypi.python.org/pypi/pyusb/1.0.0b2
usbtmc - https://pypi.python.org/pypi/python-usbtmc/0.5
vxi11 - https://pypi.python.org/pypi/python-vxi11/0.7

INSTALLATION NOTES:


usb -- PyUSB Install

On Debian Based Linux:
	Install libusb-1.0-0 with the following command:
		sudo apt-get install libusb-1.0-0

	For issues refer to:
		https://github.com/walac/pyusb

	You must add your device to the usbtmc rules file to give permission to connect:
		/etc/udev/rules.d/usbtmc.rules

		# USBTMC instruments
		# Agilent DSO-X 2004A
		SUBSYSTEMS=="usb", ACTION=="add", ATTRS{idVendor}=="0957", ATTRS{idProduct}=="179a", GROUP="usbtmc", MODE="0660"

	If you don't want to change the usbtmc rules you can run your python code with administrative priviledges with the "sudo" command refer to:
		http://dontbuyjustmake.blogspot.com/2013/12/controlling-oscilloscope-with-linux.html
