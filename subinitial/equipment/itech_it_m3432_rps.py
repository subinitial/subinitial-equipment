'''
Driver for ITECH IT-M3432 bi-directional power supply
'''
import logging
import pyvisa
import math

logger = logging.getLogger(__name__)

class ITechItM3432Rps:
    def __init__(self, host: str, min_current_a: float|None=None, max_current_a: float|None=None):
        self.host: str = host
        self.inst: pyvisa.resources.TCPIPInstrument|None = None
        self.min_current_a = min_current_a if min_current_a is not None else -math.inf
        self.max_current_a = max_current_a if max_current_a is not None else math.inf


    ################################################################################
    # Initialization and State management
    ################################################################################ 
    def initialize(self):
        '''Put the device into a known and ready to operate state'''
        self.set_remote_lock(True) # needed to run most commands. users can unlock by pressing shift + link on the supplies front panel
        self.reset()
        self.set_output(False)

    def connect(self):
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(f'TCPIP::{self.host}::30000::SOCKET', write_termination='\n', read_termination='\n') # type: ignore

    def disconnect(self):
        if self.inst is not None:
            self.set_remote_lock(False)
            self.inst.close()
            self.inst = None


    ################################################################################
    # I/O
    ################################################################################
    def scpi_write(self, cmd: str, err_check: bool=False):
        assert self.inst is not None # fail if connect hasn't been called

        logger.debug('scpi write: %s', cmd)
        self.inst.write(cmd)

        if err_check:
            errs = self.check_errors()
            if errs:
                print(f'cmd: {cmd}, errs: {errs}')

    def scpi_query(self, cmd: str, err_check: bool=False) -> str:
        assert self.inst is not None  # fail if connect hasn't been called
        
        logger.debug('scpi write: %s', cmd)
        read = self.inst.query(cmd).strip()
        logger.debug('scpi read: %s', read)

        if err_check:
            errs = self.check_errors()
            if errs:
                print(f'cmd: {cmd}, errs: {errs}')

        return read
    

    ################################################################################
    # Register Definitions
    ################################################################################
    # TODO - capture operation and questionable registers if needed ( see keysight_rp7942a_rps.py for reference)


    ################################################################################
    # Operations
    ################################################################################
    def check_errors(self) -> list[str]:
        errs: list[str] = []
        while (err:=self.scpi_query('SYST:ERR?', err_check=False)) != '0,"No error"':
            errs.append(err)

        return errs
    
    def reset(self):
        self.scpi_write('*RST')

    def get_idn(self) -> str:
        return self.scpi_query("*IDN?").strip()
    
    def set_remote_lock(self, locked: bool):
        '''
        locked = True means the device operates in remote mode
        '''
        if locked:
            self.scpi_write('SYST:REM')
            # self.scpi_write('SYST:RWL')
        else:
            self.scpi_write(f'SYST:LOC')

    def set_output(self, output_on: bool):
        self.scpi_write(f'OUTP {str(int(output_on))}')

    def set_current_limit(self, limit_amps: float):
        if not (self.min_current_a <= limit_amps <= self.max_current_a):
            raise Exception(f'Current limit out of range: {limit_amps} not between {self.min_current_a} and {self.max_current_a}')
        pos_limit = limit_amps if limit_amps > 0 else 0
        neg_limit = limit_amps if limit_amps < 0 else 0
        self.scpi_write(f'CURR:LIM {pos_limit:.2f}')
        self.scpi_write(f'CURR:LIM:NEG {neg_limit:.2f}')

    def set_voltage(self, volts: float):
        self.scpi_write(f'VOLT {volts:.2f}')

    def setup_power(self, limit_volts: float, limit_amps: float):
        self.set_output(False)
        self.set_current_limit(limit_amps)
        self.set_voltage(limit_volts)
        self.set_output(True)

    def measure_current_a(self) -> float:
        reading = self.scpi_query("MEAS:CURR?")
        return float(reading.strip())

    def measure_voltage_v(self) -> float:
        reading  = self.scpi_query("MEAS:VOLT?")
        return float(reading.strip())
        
    def measure_power(self) -> float:
        reading  = self.scpi_query("MEAS:POW?")
        return float(reading.strip())
    
    def reset_amp_hours(self):
        self.scpi_write('SENS:AHO:RES')

    def get_amp_hours(self):
        return float(self.scpi_query('FETC:AHO?'))


if __name__ == '__main__':
    psu = ITechItM3432Rps(host='172.17.32.21', min_current_a=-60, max_current_a=60)
    psu.connect()
    print(psu.check_errors())
    psu.initialize()

    psu.scpi_write('FUNC:MODE FIX')
    psu.scpi_write('SOUR:FUNC CV')

    psu.setup_power(49, 12)

    import time
    time.sleep(1)

    print(psu.scpi_query('MEAS:CURR?'), 'A')
    print(psu.scpi_query('MEAS:VOLT?'), 'V')

    psu.scpi_write('OUTP OFF')