# -*- coding: UTF-8 -*-
# Subinitial Equipment Kikusui PWX1500L series interface
# © 2012-2020 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import logging
import socket
import time

logger = logging.getLogger(__name__)


class PWX1500L:
    BLOCKING_CYLCEDELAY = 0.02  # how often to check to see if output state matches commanded state

    def __init__(self, host=None, autoconnect=True):
        """Creates a PWX1500L instance to remotely control the power supply.

        :param host: tcp/ip host as ip-address or Netbios hostname
        :type host: string
        :param autoconnect: if True the device will be connected to at instantiation
        :type autoconnect: bool
        """
        self._host = host
        self._port = 5025
        self._timeout = 5
        self._buffer_size = 2048
        self._socket = None
        self.idn = None
        self.connected = False
        if host and autoconnect:
            self.connect(host=self._host)

    def initialize(self):
        '''Put the equipment into a known and ready to work state'''
        self.reset(True)
        self.output_disable()
        self.set_current(0)
        self.set_voltage(0)

    def connect(self, host=None):
        """Connect to the device via LAN

        :param host: tcp/ip host as ip-address or Netbios hostname
        :type host: string
        :returns: True if connection was made, False if already connected to host
        :rtype: bool
        """
        if host is not None and self._host != host:
            self.close()
            self._host = host
        elif self.connected:
            return False

        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.settimeout(self._timeout)
        self._socket.connect((self._host, self._port))
        self.idn = self._ask("*IDN?")
        self.connected = True
        # if device_id != self.ID:
        #     raise Exception("Invalid Device ID? response: {}, expected {}".format(device_id, self.ID))
        return True

    def _write(self, msgstr):
        """Send command to the device but don't wait for a response

        :param msgstr: message string
        :type msgstr: str
        """
        msgstr = msgstr.strip() + "\r\n"
        self._socket.send(msgstr.encode())

    def _ask(self, msgstr):
        """Send command to the device and read response

        :param msgstr: message string
        :type msgstr: str
        :return: GPIB response string
        :rtype: str
        """
        self._write(msgstr)
        response = self._socket.recv(self._buffer_size).decode()
        return response.strip()

    def close(self):
        """Close the device connection"""
        if self.connected:
            if self._socket:
                self._socket.close()
            self._socket = None
            self.connected = False

    def __enter__(self):
        return self

    def __exit__(self):
        self.close()

    def is_operation_complete(self):
        """Get operation status of the device

        ..note: this will most likely always return True as the device will not
                respond to it until it is finished completing all of its queued operations
        :return: True if all operations are complete and device is ready for next command
        :rtype: bool
        """
        opc = self._ask("*OPC?")
        return opc != "0"

    def reset(self, wait_for_response=False):
        """Reset the device to its default state"""
        self._write("*RST")
        if wait_for_response:
            return self.is_operation_complete()
        return 0

    def _threshold(self, setpoint, measurement):
        delta = max(0.05 * abs(setpoint), 20e-3)
        # print('delta', delta, setpoint, measurement)
        return (setpoint - delta) <= measurement <= (setpoint + delta)

    def _check_status(self, vcommand, icommand):
        if vcommand is not None:
            vmeas = self.get_voltage_measured()
            if not self._threshold(vcommand, vmeas):
                return False
        if icommand is not None:
            imeas = self.get_current_measured()
            if not self._threshold(icommand, imeas):
                return False
        return True

    def set_voltage(self, commanded: float, timeout: float|None=5.0):
        self._write("VOLT {}".format(commanded))
        if timeout and self.get_output():
            timeout = max(0.5, timeout)
            if not do_for(timeout, self._check_status,
                          (commanded, None),
                          repeatdelay=self.BLOCKING_CYLCEDELAY):
                raise TimeoutError(
                    "PWX1500L commanded voltage {}V not reached within {}s".format(commanded, timeout))

    def set_current(self, commanded: float, timeout: float|None=None):
        self._write("CURR {}".format(commanded))
        if timeout and self.get_output():
            timeout = max(0.5, timeout)
            if not do_for(timeout, self._check_status,
                          (None, commanded),
                          repeatdelay=self.BLOCKING_CYLCEDELAY):
                raise TimeoutError(
                    "PWX1500L commanded current {}A not reached within {}s".format(commanded, timeout))

    def output_enable(self, timeout: float|None=5.0, wait_voltage_not_current=True):
        """Enable the output of the power supply

        ..note: timeouts < 1.0s may fail because power supply measurement settling time takes ~300ms

        :param timeout: maximum time to wait for setpoint to settle in seconds (s). Enter None to disable waiting for settling
        :type timeout: float
        :param wait_voltage_not_current: if True, wait up to timeout seconds for voltage setpoint to settle, if False wait
            up to timeout seconds for current setpoint to settle
        :type wait_voltage_not_current: bool
        """
        # FIXES SORENSEN FIRMWARE BUG: glitching on output if set_voltage is called immediately before output_enable
        time.sleep(0.025)

        self._write("OUTP {}".format(1))
        if timeout:
            timeout = max(0.5, timeout)
            if wait_voltage_not_current:
                cmd = self.get_voltage_command()
                args = cmd, None
            else:
                cmd = self.get_current_command()
                args = None, cmd
            if not do_for(timeout, self._check_status,
                          args,
                          repeatdelay=self.BLOCKING_CYLCEDELAY):
                raise TimeoutError("SornsenXPF commanded {} {} not reached within {}s".format(
                    ["current", "voltage"][wait_voltage_not_current], cmd, timeout))
        else:
            # FIXES SORENSEN FIRMWARE BUG: glitching on output if set_voltage is called immediately before output_enable
            time.sleep(0.025)

    def output_disable(self, delay: float|None=0.100):
        """disable the output of the powersupply

        :param channel: power supply channel
        :type channel: int
        :param delay: time in seconds (s)
        :type delay: float
        """
        self._write("OUTP {}".format(0))
        if delay is not None:
            time.sleep(delay)

    def get_output(self):
        """
        Identifies if a channel is enabled or not
        :return: channel status. True is on, False is off
        :rtype: bool
        """
        outstate = self._ask("OUTP?")
        return bool(int(outstate))

    def get_voltage_command(self):
        """ Get the voltage setpoint

        :param channel: power supply channel
        :type channel: int
        :return: Voltage in Volts (V)
        :rtype: float
        """
        voltage = self._ask("VOLT?")
        return float(voltage)

    def get_current_command(self):
        """ Get the current setpoint

        :return: Current in Amps (A)
        :rtype: float
        """
        current = self._ask("CURR?")
        return float(current)

    def get_voltage_measured(self):
        """ Get the measured voltage at the power supply output

        :return: Voltage in Volts (V)
        :rtype: float
        """
        vmeas = self._ask("MEAS:VOLT?")
        return float(vmeas)

    def get_current_measured(self):
        """ Get the measured current sourced by the power supply

        :return: Current in Amps (A)
        :rtype: float
        """
        imeas = self._ask("MEAS:CURR?")
        return float(imeas)

    # def is_limiting(self, channel):
    #     """Get channel v-limiting and i-limiting state
    #
    #     :return: voltage is limiting, current is limiting
    #     :rtype: bool, bool
    #     """
    #     mask = int(self._ask("LSR{}?".format(channel)))
    #     return (mask & 1) > 0, (mask & 2) > 0

    def output(self, voltage=None, current=None, enable=True, settling_time=0.1):
        """Basic function to turn on the power supply channel with a set voltage and current.
        Waits for power supply voltage to come up, then waits an extra 'settling_time' for general settling.

        :param voltage: voltage setpoint, if None setpoint will not be changed
        :type voltage: float
        :param voltage: current setpoint, if None setpoint will not be changed
        :type current: float
        :param enable: if True enables the powersupply output, if false disables the powersupply output
        :type enable: bool
        :param settling_time: seconds (s) time to wait after voltage, current, and output state have been set
        :type settling_time: float
        """
        if voltage is not None:
            self.set_voltage(commanded=voltage)
        if current is not None:
            self.set_current(commanded=current)
        if enable:
            self.output_enable()
        else:
            self.output_disable()
        time.sleep(settling_time)  # Wait for all voltages to stabilize

    def set_overvoltage_protection(self, overvoltage_protection):
        """Set overvoltage protection

        :param overvoltage_protection: overvoltage protection setpoint
        :type overvoltage_protection: float
        """
        self._write("VOLT:PROT {}".format(overvoltage_protection))

    def get_overvoltage_protection(self):
        """Get overvoltage protection

        :return overvoltage_protection: overvoltage protection setpoint
        :rtype overvoltage_protection: float
        """
        vprot = self._ask("VOLT:PROT?")
        return float(vprot)

    def set_overcurrent_protection(self, overcurrent_protection, ocp_timeout=1.0):
        """Set overcurrent protection

        :param channel: power supply channel
        :type channel: int
        :param overcurrent_protection: overcurrent protection setpoint
        :type overcurrent_protection: float
        """
        self._write("CURR:LIM:AUTO {}".format(0))
        self._write("CURR:PROT:LEV {}".format(overcurrent_protection))
        self._write("CURR:PROT:DEL {}".format(ocp_timeout))

    def get_overcurrent_protection(self):
        """Get overcurrent protection

        :return overcurrent_protection: overcurrent protection setpoint
        :rtype overcurrent_protection: float
        """
        lim = self._ask("CURR:LIM:AUTO?")
        iprot = self._ask("CURR:PROT?")
        idel = self._ask("CURR:PROT:DEL?")
        return lim, float(iprot), float(idel)

    # def clear_tripconditions(self):
    #     """Attempt to clear all trip conditions"""
    #     self._write("TRIPRST")



def do_for(timeout, func, func_args_list=None, startdelay=0, repeatdelay=0.005):
    """
    :param timeout: float, time in seconds after which
    :param func: callable, function that must return bool, or an iterable with bool as its first element.
    A return value of True indicates the function has completed successfully.
    A return value of False indicates the function must be called again after a delay "cycledelay" so long as "timeout"
    seconds have not yet elapsed since the first call to "func".
    :param func_args_list: list or tuple, arguments that will be passed to func
    :param startdelay: float, optional time in seconds to delay before starting the first call to func
    :param repeatdelay: float, time in seconds to delay before repeating the next call to func
    :return: the last value returned from func before timing out or upon func completing successfully
    """
    func_retval = None
    if startdelay > 0:
        time.sleep(startdelay)
    start_time = time.time()
    while True:
        if func_args_list is None:
            func_retval = func()
        else:
            func_retval = func(*func_args_list)
        if func_retval is not None:
            if hasattr(func_retval, '__getitem__'):
                if func_retval[0] is True:
                    break
            elif func_retval:
                break
        if time.time() - start_time >= timeout:
            break
        if repeatdelay > 0:
            time.sleep(repeatdelay)
    return func_retval
