'''
Helper/Utility code for use with a SocketCAN + python-can setup
'''

import json
from typing import Any
import logging
from pathlib import Path
import datetime
import csv
import binascii
import subprocess

import cantools.database as cantools
import can

logger = logging.getLogger(__name__)


################################################################################
# Helpers
################################################################################

def frame_decode(msg: can.Message, dbc: cantools.Database) -> dict[str, int|float]|cantools.DecodeError:
    try:
        return dbc.decode_message(msg.arbitration_id, msg.data) # type: ignore
    except cantools.DecodeError as ex:
        return ex

def setup_chan(chan_name: str, ip_link_args: str):
    '''
    The ip_link_args parameter should just contain the bus config info, "sudo ip link set {chan_name} up type can" is appended to the start of the command.

    Ex: setup_chan('can0', 'bitrate 500000 dbitrate 2000000 fd on fd-non-iso on')

    See https://docs.kernel.org/networking/can.html#the-virtual-can-driver-vcan for available args
    '''

    res = subprocess.run(f"sudo ip link set {chan_name} down", shell=True, capture_output=True, text=True)
    if res.returncode != 0:
        raise Exception(f"Failed to reset CAN channel. Chan: {chan_name}, ip link output: {res.stdout}, {res.stderr}")
    res = subprocess.run(f"sudo ip link set {chan_name} up type can {ip_link_args}", shell=True, capture_output=True, text=True)
    if res.returncode != 0:
        raise Exception(f"Failed to reset CAN channel. Chan: {chan_name}, ip link output: {res.stdout}, {res.stderr}")

def disable_chan(chan_name: str):
    res = subprocess.run(f"sudo ip link set {chan_name} down", shell=True, capture_output=True, text=True)
    if res.returncode != 0:
        raise Exception(f"Failed to shut down CAN channel. Chan: {chan_name}, ip link output: {res.stdout}, {res.stderr}")

################################################################################
# python-can Listners
################################################################################
class DbcCsvListner(can.Listener):
    def __init__(self, log_dir: str|Path, dbc_file: str|Path|None=None, pretty_print: bool=False):
        self.dbc: cantools.Database|None = cantools.load_file(str(dbc_file)) if dbc_file is not None else None # type: ignore
        self.json_indent = 4 if pretty_print else None

        self.log_dir = Path(log_dir).resolve()
        if not Path(log_dir).exists():
            self.log_dir.mkdir(parents=True)

        self.csv_file = open(Path(self.log_dir, f'can_log_{datetime.datetime.now().strftime("%y-%m-%d %H_%M_%S")}.csv'), mode='w', newline='')
        self.writer = csv.DictWriter(self.csv_file, ['Timestamp', 'ID', 'Chan', 'Tx/Rx', 'Data', 'Parsed Data']) 
        self.writer.writeheader()

    def on_message_received(self, msg: can.Message) -> None:
        self.writer.writerow({
            'Timestamp': repr(msg.timestamp),
            'ID': hex(msg.arbitration_id),
            'Chan': str(msg.channel),
            'Tx/Rx': 'Rx' if msg.is_rx else 'Tx',
            'Data': binascii.hexlify(msg.data, ' '),
            'Parsed Data': json.dumps(frame_decode(msg, self.dbc), cls=_CustomEncoder, indent=self.json_indent) if self.dbc else ''
        })
class _CustomEncoder(json.JSONEncoder):
    def default(self, o: Any) -> str|float|int:
        if isinstance(o, cantools.signal.NamedSignalValue):
            return o.__repr__()
        else:
            return super().default(o)
