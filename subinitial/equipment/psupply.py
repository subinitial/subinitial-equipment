# Copyright Subinitial LLC, Author: Kyle Howen
from subinitial.equipment.utils import do_for, time


class PsupplyBase:
    """
    Abstract Power-Supply base class
    """
    DEFAULT = None
    AUTO = None
    SLEEP = time.sleep

    def __init__(self, host: str = None, name: str = AUTO):
        self.default_channel = 1
        """ particular default channel, modifiable by the user per psupply instance. """
        self.host: str = host
        """ hostname/ipv4/connection stringused in the implementations connect() method, e.g. '172.17.4.2' """
        self.name = name or type(self).__name__
        """ optional uniqe identifying name for this power-supply instance """

    def connect(self):
        """ Open an active connection to the psupply hardware """
        raise NotImplementedError()
    
    def close(self):
        """ Close an active conneciton to the psupply hardware (or fail quietly if not connected) """
        raise NotImplementedError()
    
    def set_output(self, state: bool, channel=DEFAULT):
        """
        @param state: True is active, False is off
        @param channel: power-supply channel being accessed
        """
        raise NotImplementedError()

    def get_output(self, channel=DEFAULT) -> bool:
        """
        @param channel: power-supply channel being accessed
        @return: channel active state as True: active, False: off
        """
        raise NotImplementedError()
    
    def set_vlimit(self, setpoint: float, channel=DEFAULT):
        """
        @param setpoint: voltage limit setpoint in Volts (V)
        @param channel: power-supply channel being accessed
        """
        raise NotImplementedError()
    
    def get_vlimit(self, channel=DEFAULT) -> float:
        """
        @param channel: power-supply channel being accessed
        @return: vlimit setpoint in volts (V)
        """
        raise NotImplementedError()
    
    def measure_voltage(self, channel=DEFAULT) -> float:
        """
        @param channel: power-supply channel being accessed
        @return: channel voltage output measurement in volts (V)
        """
        raise NotImplementedError()

    def set_ilimit(self, setpoint: float, channel=DEFAULT):
        """
        @param setpoint: current limit setpoint in Amps (A)
        @param channel: power-supply channel being accessed
        """
        raise NotImplementedError()

    def get_ilimit(self, channel=DEFAULT) -> float:
        """
        @param channel: power-supply channel being accessed
        @return: ilimit setpoint in amps (A)
        """
        raise NotImplementedError()

    def measure_current(self, channel=DEFAULT) -> float:
        """
        @param channel: power-supply channel being accessed
        @return: channel current output measurement in amps (A)
        """
        raise NotImplementedError()

    def wait_for_voltage(self, v_target: float=AUTO, channel=DEFAULT, v_threshold: float=AUTO, timeout: float=3.0, polling_delay=0.05):
        """
        Wait for the psupply channel to reach with v_threshold of v_target volts for at least timeout seconds.

        @v_target: target quiescient voltage (V), if AUTO the vlimit will be used
        @v_threshold: +/- absolute tolerance between measured and target voltage (V)
        @param channel: power-supply channel being accessed
        @param timeout: time (s) after which to consider the operation unsuccessful and TimeoutException will be thrown.
        @param polling_delay: how often in seconds the psupply channel is measured to determine voltage proximity.
        """
        if v_target is self.AUTO:
            v_target = self.get_vlimit(channel=channel)
        if v_threshold is self.AUTO:
            v_threshold = max(0.05 * v_target, 0.1)
        v_threshold = abs(v_threshold)

        if not do_for(
            timeout, 
            lambda: abs(self.measure_voltage(channel) - v_target) < v_threshold,
            repeatdelay=polling_delay, sleep=self.SLEEP):
            raise TimeoutError(f"{self.name}: commanded voltage not reached within {timeout}s")
