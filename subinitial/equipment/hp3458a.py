# -*- coding: UTF-8 -*-
# Subinitial Test Automation Framework HP3458A series interface
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import logging
import socket
import time

logger = logging.getLogger(__name__)

class HP3458A:
    ID = "HP3458A"

    @property
    def is_connected(self):
        return self.socket is not None

    def __init__(self, host=None, port=1234, gpib_id=22, timeout=5.0, autoconnect=True):
        self.host = host
        self.port = port
        self.timeout = timeout
        self.gpib_id = gpib_id
        self._buffer_size = 2048
        self.mode = None
        self.range = None
        self.powerline_cycles = None
        self.socket = None
        if host and port and autoconnect:
            self.connect()

    def set_gpib_id(self, gpib_id):
        self.gpib_id = gpib_id
        self.write("++addr {}".format(self.gpib_id))

    def get_gpib_id(self):
        self.gpib_id = int(self.ask("++addr"))
        return self.gpib_id

    # Voltages
    MODE_DCV = "DCV"
    MODE_ACV = "ACV"
    MODE_ACDCV = "ACDCV"
    # Currents
    MODE_DCI = "DCI"
    MODE_ACI = "ACI"
    MODE_ACDCI = "ACDCI"
    # Resistances
    MODE_OHM = "OHM"  # 2 Wire Ohm Measurement
    MODE_OHMF = "OHMF"  # Four Wire Ohm Measurement

    RANGE_AUTO = -1

    RANGE_DCV_0V100 = 0.1
    RANGE_DCV_1V = 1
    RANGE_DCV_10V = 10
    RANGE_DCV_100V = 100
    RANGE_DCV_1000V = 1000

    RANGE_ACV_0V010 = 0.01
    RANGE_ACV_0V100 = 0.1
    RANGE_ACV_1V = 1
    RANGE_ACV_10V = 10
    RANGE_ACV_100V = 100
    RANGE_ACV_1000V = 1000

    RANGE_DCI_0uA1 = .1e-6
    RANGE_DCI_1uA = 1e-6
    RANGE_DCI_10uA = 10e-6
    RANGE_DCI_100uA = 100e-6
    RANGE_DCI_1mA = 1e-3
    RANGE_DCI_10mA = 10e-3
    RANGE_DCI_100mA = 100e-3
    RANGE_DCI_1A = 1

    RANGE_ACI_100uA = 100e-6
    RANGE_ACI_1mA = 1e-3
    RANGE_ACI_10mA = 10e-3
    RANGE_ACI_100mA = 100e-3
    RANGE_ACI_1A = 1

    RANGE_OHM_10R = 10
    RANGE_OHM_100R = 100
    RANGE_OHM_1kR = 1e3
    RANGE_OHM_10kR = 10e3
    RANGE_OHM_100kR = 100e3
    RANGE_OHM_1MR = 1e6
    RANGE_OHM_10MR = 10e6
    RANGE_OHM_100MR = 100e6
    RANGE_OHM_1GR = 1e9

    def set_mode(self, mode, range_=RANGE_AUTO, powerline_cycles=6):
        # if self.mode != mode or self.range != range_:
        self.mode = mode
        self.range = range_
        self.write("{} {}".format(mode, range_))
        # if self.powerline_cycles != powerline_cycles:
        self.powerline_cycles = powerline_cycles
        self.write("NPLC {}".format(powerline_cycles))

    def connect(self, host=None):
        """Connect to the device via LAN

        :param host: tcp/ip host as ip-address or Netbios hostname
        :type host: string
        :returns: True if connection was made, False if already connected to host
        :rtype: bool
        """
        if host is not None and self.host != host:
            self.close()
            self.host = host
        elif self.is_connected:
            return False
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(self.timeout)
        self.socket.connect((self.host, self.port))
        self.set_gpib_id(self.gpib_id)
        self._setup_trigger()

        tries = 3
        while True:
            try:
                device_id = self.ask("ID?")
                if device_id != self.ID:
                    raise Exception("Invalid Device ID? response: {}, expected {}".format(device_id, self.ID))
                else:
                    break
            except Exception as ex:
                if tries:
                    tries -= 1
                    print("Invalid Device ID, retrying connection to HP3458a")
                else:
                    self.close()
                    raise ex
        return True

    TRIGGER_AUTO = 1
    TRIGGER_EXT = 2
    TRIGGER_SGL = 3
    TRIGGER_HOLD = 4
    TRIGGER_SYN = 5

    def _setup_trigger(self, source=TRIGGER_AUTO, number_arms=1):
        msg = "TARM {}".format(source)
        if source == self.TRIGGER_SGL:
            msg += " {}".format(number_arms)
        self.write(msg)
        self.write("T 3")
        self._clear_socket()  # clear any leftover readings in the buffer

    def _clear_socket(self):
        self.socket.settimeout(0.1)
        time.sleep(0.1)
        try:
            self.socket.recv(self._buffer_size)
        except:
            pass
        self.socket.settimeout(self.timeout)

    def write(self, msgstr):
        """Send command to HP3458A DMM but don't wait for a response

        :param msgstr: GPIB message string
        :type msgstr: str
        """
        msgstr = msgstr.strip() + "\r\n"
        self.socket.send(msgstr.encode())

    def ask(self, msgstr):
        """Send command to 3458A DMM and read response

        :param msgstr: GPIB message string
        :type msgstr: str
        :return: GPIB response string
        :rtype: str
        """
        self.write(msgstr)
        response = self.socket.recv(self._buffer_size).decode()
        return response.strip()

    def close(self):
        if self.socket:
            self.socket.shutdown(socket.SHUT_RDWR)
            self.socket.close()
            self.socket = None

    def __enter__(self):
        return self

    def __exit__(self):
        self.close()

    def configure_autozero(self, on_not_off=True):
        self.write("AZERO {}".format(["OFF", "ON"][on_not_off]))

    AUTORANGE_OFF = 0
    AUTORANGE_ON = 1
    AUTORANGE_ONCE = 2

    def configure_autorange(self, autorange=AUTORANGE_ON):
        self.write("ARANGE {}".format(autorange))

    def get_autorange(self):
        return self._tryask("ARANGE?", int)  # int(self.ask("ARANGE?"))

    def get_measurement(self):
        return self._tryask("T 3", float)

    def _tryask(self, question, conv):
        retries = 2
        while True:
            try:
                return conv(self.ask(question))
            except ValueError as ex:
                if retries <= 0:
                    raise ex
            retries -= 1

    def get_voltage(self, range_=RANGE_AUTO, powerline_cycles=6):
        self.set_mode(self.MODE_DCV, range_, powerline_cycles)
        # response = self.ask("T 3")  # TRIG SGL
        return self.get_measurement()

    def get_acvoltage(self, range_=RANGE_AUTO, powerline_cycles=6):
        self.set_mode(self.MODE_ACV, range_, powerline_cycles)
        # response = self.ask("T 3")  # TRIG SGL
        return self.get_measurement()

    def get_current(self, range_=RANGE_AUTO, powerline_cycles=6):
        self.set_mode(self.MODE_DCI, range_, powerline_cycles)
        # response = self.ask("T 3")  # TRIG SGL
        return self.get_measurement()

    AUTOCAL_ALL = 0
    AUTOCAL_DCV = 1  # Do this before any others, ALL does this first automatically
    AUTOCAL_AC = 2
    AUTOCAL_OHMS = 4

    def autocal(self, caltype=AUTOCAL_ALL):
        print("autocal: start", caltype)

        input_val = input("HP3458 Autocal requested, please disconnect the DMM then press enter or 's' to skip...")
        if input_val.strip().lower() != "s":
            self.write("ACAL {}".format(caltype))
            autocal_time = self.autocal_times[caltype]

            # sleep_interval = 2
            # while autocal_time > 0:
            #     time.sleep(sleep_interval)
            #     autocal_time -= sleep_interval
            #     print("autocal: {}s remain".format(autocal_time))

            input("HP3458 Autocal Started. (ETA > {} minutes) When complete: "
                  "!!!!!>>>>RECONNECT DMM<<<<!!!!! then press enter... ".format(autocal_time/60))

        try:
            id = self.ask("ID?")
            if id != self.ID:
                raise Exception("Invalid id, expected: {}, received {}".format(self.ID, id))
        except Exception as ex:
            print("autocal: failed to complete autocal")
            print(ex)

        print("autocal: return")

    autocal_times = {
        AUTOCAL_ALL: 11*60,
        AUTOCAL_DCV: 1*60,
        AUTOCAL_AC: 1*60,
        AUTOCAL_OHMS: 10*60, }

    def get_temp(self):
        """
        :return: degrees Centigrade
        """
        while True:
            stemp = None
            try:
                stemp = self.ask("TEMP?")
                return float(stemp)
            except Exception as ex:
                print(stemp, ex)

        return float(self.ask("TEMP?"))

    PRESET_NORM = 1
    PRESET_FAST = 0
    PRESET_DIG = 2

    def preset(self, preset_type=PRESET_NORM):
        self.write("PRESET {}".format(preset_type))

    def trig_hold(self):
        self.write("TRIG HOLD")

    def trig_single(self):
        self.write("TRIG SGL")


if __name__ == "__main__":

    # Example Usage
    dmm = HP3458A()
    while True:
        cmd = next(iter(input(">")), " ").lower()
        if cmd == "v":
            print(dmm.get_voltage())
        elif cmd == "i":
            print(dmm.get_current())
        elif cmd == "a":
            print(dmm.get_acvoltage())
