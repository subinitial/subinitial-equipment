'''
Driver for Keysight RP7942A
'''
import logging
import math
import pyvisa
from enum import IntFlag


logger = logging.getLogger(__name__)


class KeySightRp7942aRps:
    def __init__(self, host: str, min_current_a: float|None=None, max_current_a: float|None=None):
        self.host = host
        self.inst: pyvisa.resources.TCPIPInstrument|None = None
        self.min_current_a = min_current_a if min_current_a is not None else -math.inf
        self.max_current_a = max_current_a if max_current_a is not None else math.inf


    ################################################################################
    # Initialization and State management
    ################################################################################
    def initialize(self):
        '''Put the device into a known and ready to operate state'''
        self.reset()
        self.set_output(False)

    def connect(self):
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(f'TCPIP::{self.host}::INSTR', read_termination='\n', write_termination='\n') # type: ignore

    def disconnect(self):
        if self.inst is not None:
            self.inst.close()
            self.inst = None


    ################################################################################
    # I/O
    ################################################################################
    def scpi_write(self, cmd: str):
        assert self.inst is not None # fail if connect hasn't been called

        logger.debug('scpi write: %s', cmd)
        self.inst.write(cmd)

    def scpi_query(self, cmd: str) -> str:
        assert self.inst is not None  # fail if connect hasn't been called
        
        logger.debug('scpi write: %s', cmd)
        read = self.inst.query(cmd).strip()
        logger.debug('scpi read: %s', read)

        return read


    ################################################################################
    # Register Definitions
    ################################################################################
    class OpReg(IntFlag):
        CV = 1            # Output is in constant voltage
        CC = 2            # Output is in constant current
        OFF = 4           # Output is programmed off
        WTG_MEAS = 8      # Measurement system is waiting for a trigger
        WTG_TRAN = 16     # Transient system is waiting for a trigger
        MEAS_ACTIVE = 32  # Measurement system is initiated or in progress
        TRAN_ACTIVE = 64  # Transient system is initiated or in progress

    def get_operation_status_reg(self) -> OpReg:
        return self.OpReg(int(self.scpi_query('STAT:OPER:COND?')))


    class Ques1Reg(IntFlag):
        OV = 1    # Output is disabled by the over-voltage protection
        OC = 2    # Output is disabled by the over-current protection
        PF = 4    # Output is disabled by power-fail (low-line or brownout on AC line)
        CP_PLUS = 8    # Output is disabled by the positive over-power limit
        OT = 16   # Output is disabled by the over-temperature protection
        CP_MINUS = 32  # Output is disabled by the negative over-power limit
        OV_MINUS = 64  # Output is disabled by a negative OV due to reversed sense leads
        LIM_PLUS = 128  # Output is in positive voltage or current limit
        LIM_MINUS = 256  # Output is in positive voltage or negative current limit
        INH = 512  # Output is disabled by an external INHibit signal
        UNR = 1024  # Output is unregulated
        PROT = 2048  # Output is disabled by a watchdog timer protection
        EDP = 4096  # Output is disabled by excessive output dynamic protection

    def get_questionable_1_status_reg(self) -> Ques1Reg:
        return self.Ques1Reg(int(self.scpi_query('STAT:QUES1:COND?')))


    class Ques2Reg(IntFlag):
        IPK_PLUS = 2   # Output is in positive peak current limit
        IPK_MINUS = 4  # Output is in negative peak current limit
        CSF = 8        # A current sharing fault has occurred
        PSP = 16       # Output is disabled by a primary/secondary protection
        SDP = 32       # Output is disabled by a Safety Disconnect System protection
        UV = 64        # An under-voltage protection has occurred
        OCF = 128      # An internal over-current fault has occurred
        LOV = 256      # An internal over-voltage fault has occurred
        DOV = 512      # A DUT-applied over-voltage fault has occurred

    def get_questionable_2_status_reg(self) -> Ques2Reg:
        return self.Ques2Reg(int(self.scpi_query('STAT:QUES2:COND?')))


    ################################################################################
    # Operations
    ################################################################################
    def check_errors(self) -> list[str]:
        errs: list[str] = []
        while (err:=self.scpi_query('SYST:ERR?')) != '+0,"No error"':
            errs.append(err)

        return errs
    
    def reset(self):
        self.scpi_write('*RST')

    def get_idn(self) -> str:
        return self.scpi_query("*IDN?").strip()

    def set_remote_lock(self, locked: bool):
        '''
        locked = True means the device operates in remote mode
        '''
        self.scpi_write(f'SYST:LOCK {str(int(locked))}')

    def set_output(self, output_on: bool):
        self.scpi_write(f'OUTP {str(int(output_on))}')

    def set_current_limit(self, limit_amps: float):
        if not (self.min_current_a <= limit_amps <= self.max_current_a):
            raise Exception(f'Current limit out of range: {limit_amps} not between {self.min_current_a} and {self.max_current_a}')
        pos_limit = limit_amps if limit_amps > 0 else 0
        neg_limit = limit_amps if limit_amps < 0 else 0
        self.scpi_write(f'CURR:LIM {pos_limit:.2f}')
        self.scpi_write(f'CURR:LIM:NEG {neg_limit:.2f}')

    def set_voltage(self, volts: float):
        self.scpi_write(f'VOLT {volts:.2f}')

    def setup_power(self, limit_volts: float, limit_amps: float):
        self.set_output(False)
        self.set_current_limit(limit_amps)
        self.set_voltage(limit_volts)
        self.set_output(True)

    def measure_current_a(self) -> float:
        reading = self.scpi_query("MEAS:CURR?")
        return float(reading.strip())

    def measure_voltage_v(self) -> float:
        reading  = self.scpi_query("MEAS:VOLT?")
        return float(reading.strip())
        
    def measure_power(self) -> float:
        reading  = self.scpi_query("MEAS:POW?")
        return float(reading.strip())

    def reset_amp_hours(self):
        self.scpi_write('SENS:AHO:RES')

    def get_amp_hours(self):
        return float(self.scpi_query('FETC:AHO?'))