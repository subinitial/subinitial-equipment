# -*- coding: UTF-8 -*-
# Subinitial Equipment Library Thermotron 8200 series interface
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import logging
import pyvisa

logger = logging.getLogger(__name__)


class Thermotron8200:
    """ Simple Python adapter for Thermotron 8200+ Modules """

    def __init__(self, host: None|str=None, port: None|str|int=None):
        """ Establish connection parameters but do not perform connection just yet """
        self.host = host
        self.port = port
        self.inst: pyvisa.resources.TCPIPInstrument|None = None

    def initialize(self):
        '''Set the equipment in a known and ready to work state'''
        assert self.inst is not None

        self.inst.clear()

        self.stop()

        # NOTE - it seems there isn't a way to set the units via SCPI
        if units:=self.query('CCHR1?').lower() != 'c':
            raise Exception(f'TT8200 Not configured to use Celsius, reported units: {units}. Please configure the unit to Celsius mode.')


    ################################################################################
    # I/O and connection
    ################################################################################
    def connect(self, host: None|str=None, port: None|str=None):
        """ Connect to the device """
        if host is not None:
            self.host = host
        if port is not None:
            self.port = port

        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(f'TCPIP::{self.host}{"::"+str(self.port) if self.port is not None else ""}::SOCKET', read_termination='\r', write_termination='\r')
        self.inst.timeout = 1000 # ms

    def close(self):
        """ Disconnect from the device """
        if self.inst is not None:
            self.inst.close()
            self.inst = None

    def __enter__(self):
        """ Enable 'with' syntax """
        self.connect()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """ Enable 'with' syntax """
        self.close()

    def write(self, command: str):
        """ Send a command to the device """
        logger.debug('writing: %s', command)
        self.inst.write(command)

    def query(self, command: str) -> str:
        """ Send a command to the device and then read the response """
        assert self.inst is not None

        logger.debug('writing: %s', command)
        reply = self.inst.query(command).strip()
        logger.debug('read: %s', reply)
        return reply

    def command(self, cmd_str: str) -> bool:
        assert self.inst is not None

        resp = self.inst.query(cmd_str).strip()
        if resp != '0':
            logger.warning('Error response command: %s, response: %s', cmd_str, resp)
            return False
        else:
            return True

    ################################################################################
    # Command Wrappers
    ################################################################################
    # Control Commands
    def stop(self):
        return self.command('STOP')

    def runm(self):
        return self.command('RUNM')

    # Program Status Commands


    # Programming Commands

    # System Status Commands
    def get_idn(self) -> str:
        """ Ask device for identifying information """
        return self.query("*IDN?")

    # Variable Commands
    def setp(self, temp: float, chan: int=1):
        return self.command(f'SETP{chan},{temp:3.1f}')

    def get_pvar(self, chan: int=1):
        return self.query(f'PVAR{chan}?')

    def set_ramp_rate(self, rate: int):
        '''rate is in the current units of the channel, default is celcius per minute'''
        return self.command(f'MRMP1,{rate}')

    def go_to_temp(self, temp_c: float, ramp_rate: float, ptc=False):
        '''Approach target temp at selected ramp rate'''
        self.stop()
        
        if ptc:
            self.command(f'SETP3,{temp_c:3.1f}')
            self.command(f'MRMP3,{ramp_rate:3.1f}')
            self.command(f'OPTN,129')
        else:
            self.command(f'SETP1,{temp_c:3.1f}')
            self.command(f'MRMP1,{ramp_rate:3.1f}')
            self.command(f'OPTN,128')
        
        self.runm()

    def get_air_temp(self):
        return float(self.query(f'PVAR1?'))
    
    def get_product_temp(self):
        return float(self.query(f'PVAR3?'))

if __name__ == "__main__":
    """ Debug Entrypoint """

    tt8200 = Thermotron8200(host='127.127.127.1::8888')
    tt8200.connect()

    print(tt8200.get_idn())
