# -*- coding: UTF-8 -*-
# Subinitial Test Automation Framework rigolds1000 series interface
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import logging

logger = logging.getLogger(__name__)

class RigolDS1000:
    vid = 0x1AB1
    pid = 0x0588
    _mode_usb = "USB"
    _mode_dc = "DISCONNECTED"
    _mode_tcpip = "TCPIP"
    _retrycount = 3

    def __init__(self, serial=None):
        self.serial = serial
        self.mode = RigolDS1000._mode_dc
        self.idn = None
        self.ipaddr = None
        self.device = None
        """:type : usbtmc.Instrument"""

        self.aquire = self.Aquire()
        self.trigger = self.Trigger()
        self.math = self.Math()

    def is_connected(self):
        return self.idn is not None

    def connect_usb(self):
        self.mode = RigolDS1000._mode_usb
        retries = 0
        import usbtmc
        while True:
            try:
                if self.serial is None:
                    self.device = usbtmc.Instrument(RigolDS1000.vid, RigolDS1000.pid)
                    self.math.device = self.trigger.device = self.aquire.device = self.device
                else:
                    self.device = usbtmc.Instrument(RigolDS1000.vid, RigolDS1000.pid, iSerial=self.serial)
                    self.math.device = self.trigger.device = self.aquire.device = self.device
                break
            except Exception as ex:
                retries += 1
                logger.info("connect failed on try {} with exception: {}".format(retries, ex))
                if retries > RigolDS1000._retrycount:
                    raise ex
        self.device.write(" ")
        self.idn = self._get_idn()

    def connect_tcpip(self, ipaddress):
        self.mode = RigolDS1000._mode_tcpip
        self.ipaddr = ipaddress
        import vxi11
        retries = 0
        while True:
            try:
                self.device = vxi11.Instrument(self.ipaddr)
                self.idn = self._get_idn()
                break
            except Exception as ex:
                retries += 1
                logger.info("connect failed on try {} with exception: {}".format(retries, ex))
                if retries > RigolDS1000._retrycount:
                    raise ex

    def _get_idn(self):
        retries = 0
        while True:
            try:
                return self.device.ask("*IDN?")
            except Exception as ex:
                retries += 1
                logger.info("communication for *IDN? failed on try {} with exception: {}".format(retries, ex))
                if retries > RigolDS1000._retrycount:
                    raise ex

    def close(self):
        self.idn = None
        import usb.util
        if self.mode == self._mode_usb and self.device is not None:
            self.device.reset()  # TODO: this method does nothing, find a way to reset the SCPI command interface
            usb.util.release_interface(self.device.device, self.device.iface)

    def __enter__(self):
        return self

    def __exit__(self):
        self.close()

    def reset(self):
        self.device.write("*RST")

    # DEVICE SPECIFIC CODE BELOW
    def run(self):
        self.device.write(":RUN")

    def stop(self):
        self.device.write(":STOP")

    def auto(self):
        self.device.write(":AUTO")

    def force_trigger(self):
        self.device.write(":FORCetrig")

    def hardcopy(self):
        self.device.write(":HARDcopy")

    CHANNEL_1 = "CHANnel1"
    CHANNEL_2 = "CHANnel2"
    CHANNEL_3 = "CHANnel3"
    CHANNEL_4 = "CHANnel4"
    CHANNEL_DIGITAL = "DIGital"
    CHANNEL_MATH = "MATH"
    CHANNEL_FFT = "FFT"

    def get_vpp(self, channel):
        return float(self.device.ask(":MEASure:VPP?"))

    def get_voffset(self, channel_idx):
        return float(self.device.ask(":CHANnel{}:OFFSet?".format(channel_idx)))

    def get_vscale(self, channel_idx):
        return float(self.device.ask(":CHANnel{}:SCALe?".format(channel_idx)))

    def get_waveform(self, channel_idx):
        self.device.write(":WAVeform:DATA? CHANnel{}".format(channel_idx))
        data = self.device.read_raw()
        offset = self.get_voffset(channel_idx)
        scale = self.get_vscale(channel_idx)

        return list(map(lambda x: ((int(x)-128)/255 - offset)*scale, data[10:]))


    class Aquire:
        def __init__(self):
            self.device = None

        TYPE_NORMAL = "NORMAL"
        TYPE_AVERAGE = "AVERAGE"
        TYPE_PEAKDETECT = "PEAKDETECT"

        def get_type(self):
            return self.device.ask(":ACQuire:TYPE?")

        def set_type(self, type_):
            self.device.write(":ACQuire:TYPE " + type_)

        MODE_REALTIME = "RTIMe"
        MODE_EQUIVALENTSAMPLING = "ETIMe"

        def get_mode(self):
            return self.device.ask(":ACQuire:MODE?")

        def set_mode(self, mode):
            self.device.write(":ACQuire:MODE " + mode)

        AVERAGING_2 = 2
        AVERAGING_4 = 4
        AVERAGING_8 = 8
        AVERAGING_16 = 16
        AVERAGING_32 = 32
        AVERAGING_64 = 64
        AVERAGING_128 = 128
        AVERAGING_256 = 256

        def get_averaging(self):
            return int(self.device.ask(":ACQuire:AVER?"))

        def set_averaging(self, averaging):
            self.device.write(":ACQuire:AVER " + averaging)

    class Trigger:
        def __init__(self):
            self.device = None
            self.mode = self.MODE_EDGE

        MODE_EDGE = "EDGE"
        MODE_PULSE = "PULSE"
        MODE_VIDEO = "VIDEO"
        MODE_SLOPE = "SLOPE"
        MODE_PATTERN = "PATTERN"
        MODE_DURATION = "DURATION"
        MODE_ALTERNATION = "ALTERNATION"

        def get_mode(self):
            """gets trigger mode

            :rtype: string
            """
            self.mode = self.device.ask(":TRIG:MODE?")
            return self.mode

        def set_mode(self, mode):
            """sets trigger mode

            :param mode: refer to Trigger.MODE_* options
            """
            self.mode = mode
            self.device.write(":TRIG:MODE " + mode)

        SOURCE_CHANNEL1 = "CHANnel1"
        SOURCE_CHANNEL2 = "CHANnel2"
        SOURCE_CHANNEL3 = "CHANnel3"
        SOURCE_CHANNEL4 = "CHANnel4"
        SOURCE_EXT = "EXT"
        SOURCE_ACLINE = "ACLine"

        def get_source(self):
            return self.device.ask("TRIGger:{}:SOURce?".format(self.mode))

        def set_source(self, source):
            self.device.write("TRIGger:{}:SOURce {}".format(self.mode, source))

        def get_level(self):
            return self.device.ask("TRIGger:{}:LEVel?".format(self.mode))

        def set_level(self, level):
            """set trigger level

            :param level: trigger level from -6.0 to 6.0 of Scale (V/div)
            :type level: float
            """
            self.device.write("TRIGger:{}:LEVel {}".format(self.mode, level))

        SWEEP_AUTO = "AUTO"
        SWEEP_NORMAL = "NORMAL"
        SWEEP_SINGLE = "SINGLE"

        def get_sweep(self):
            return self.device.ask("TRIGger:{}:SWEep?".format(self.mode))

        def set_sweep(self, sweep):
            self.device.write("TRIGger:{}:SWEep {}".format(self.mode, sweep))

        COUPLING_DC = "DC"
        COUPLING_AC = "AC"
        COUPLING_HF = "HF"
        COUPLING_LF = "LF"

        def get_coupling(self):
            return self.device.ask("TRIGger:{}:COUPling?".format(self.mode))

        def set_coupling(self, sweep):
            self.device.write("TRIGger:{}:COUPling {}".format(self.mode, sweep))

        def get_holdoff(self):
            return self.device.ask("TRIGger:HOLDoff?")

        def set_holdoff(self, holdoff):
            """set trigger holdoff time

            :param holdoff: holdoff time from 500e-9 to 1.5 seconds
            :type holdoff: float
            """
            self.device.write("TRIGger:HOLDoff {}".format(holdoff))

        STATUS_RUN = "RUN"
        STATUS_STOP = "STOP"
        STATUS_TRIGGERED = "T'D"
        STATUS_WAIT = "WAIT"
        STATUS_AUTO = "AUTO"

        def get_status(self):
            return self.device.ask("TRIGger:STATus?")

        def set_to_50(self):
            """Set trigger level to vertical midpoint of amplitude"""
            self.device.write(":Trig%50")

        def force(self):
            """Force oscilloscope to trigger signal in Normal/Single mode"""
            self.device.write(":FORCetrig")

        EDGE_POSITIVE = "POSITIVE"
        EDGE_NEGATIVE = "NEGATIVE"

        def get_edge_slope(self):
            return self.device.ask("TRIGger:{}:SLOPe?".format("EDGE"))

        def set_edge_slope(self, slope):
            self.device.write("TRIGger:{}:SLOPe {}".format("EDGE", slope))

        def get_edge_sensitivity(self):
            return self.device.ask("TRIGger:{}:SENSitivity?".format("EDGE"))

        def set_edge_sensitivity(self, sensitivity):
            """set edge trigger sensitivity

            :param sensitivity: from 0.1div to 1div
            :type sensitivity: float
            """
            self.device.write("TRIGger:{}:SENSitivity {}".format("EDGE", sensitivity))

    class Math:
        def __init__(self):
            self.device = None

        DISPLAY_ON = "ON"
        DISPLAY_OFF = "OFF"

        def get_display(self):
            return self.device.ask(":MATH:DISPlay?")

        def set_display(self, display):
            self.device.write(":MATH:DISPlay " + display)

        OPERATION_A_PLUS_B = "A+B"
        OPERATION_A_MINUS_B = "A-B"
        OPERATION_A_TIMES_B = "A*B"
        OPERATION_FFT = "FFT"

        def get_operation(self):
            return self.device.ask(":MATH:OPERate?")

        def set_operation(self, operation):
            self.device.write(":MATH:OPERate " + operation)

        def get_fft_display(self):
            return self.device.ask(":FFT:DISPlay?")

        def set_fft_display(self, display):
            self.device.write(":FFT:DISPlay " + display)
