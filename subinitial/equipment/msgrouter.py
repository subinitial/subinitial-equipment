# -*- coding: UTF-8 -*-
# Subinitial Test Automation Framework message router for sharing equipment across a network
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import socketserver
import threading
import queue
import logging

# from ..test.utils import setup_logging
logger = logging.getLogger(__name__)


class TcpMsgHandler(socketserver.BaseRequestHandler):
    def handle(self):
        router = self.server.router
        router.add_client(self.request)
        while True:
            try: data = self.request.recv(2048)
            except ConnectionError: break
            if not data: break
            method_name, msg = data.decode().split(',', 1)
            router.queue_request(self.request, method_name, msg)
        router.remove_client(self.request)


class MsgRouter:
    def __init__(self, device, port=8091, host="0.0.0.0", threaded=True):
        self.host = host
        self.port = port
        self.device = device
        self.clients = []
        self.request_queue = queue.Queue()
        self.response_queue = queue.Queue()

        # Tcp Server Task
        self.tcp_server = socketserver.ThreadingTCPServer((host, port), TcpMsgHandler, True)
        self.tcp_server.router = self
        self.tcp_thread = threading.Thread(target=self.tcp_server.serve_forever)
        self.tcp_thread.daemon = True

        self.threaded = threaded

    def start(self):
        self.tcp_thread.start()
        self.routertask()

    def rx_handler(self, buf):
        self.response_queue.put(buf)

    def add_client(self, clientsocket):
        if clientsocket not in self.clients:
            self.clients.append(clientsocket)
            logger.info("Client connected: {} total".format(len(self.clients)))

    def remove_client(self, clientsocket):
        if clientsocket in self.clients:
            self.clients.remove(clientsocket)
            logger.info("Client disconnected: {} remain".format(len(self.clients)))

    def queue_request(self, tcp_request, method_name, data):
        self.request_queue.put((tcp_request, method_name, data))

    def routertask(self):
        while True:
            socket, method_name, request = self.request_queue.get()

            # Send message to the device and get / generate a response
            if method_name.startswith("w"):
                getattr(self.device, method_name)(request)
                response = '_'
            else:
                response = getattr(self.device, method_name)(request)

            try: socket.sendall(response.encode())
            except OSError:
                logger.warning("client closed connection before response was sent")
                self.remove_client(socket)

# Example usage below.
if __name__ == "__main__":
    from .rigoldp800 import RigolDP800
    myrigol = RigolDP800() # serial=DM3O153500563)
    myrigol.connect_usb()
    # myrigol.connect_tcpip("192.168.1.76")
    mydevice_router = MsgRouter(myrigol.device, threaded=False)
    mydevice_router.start()
