# -*- coding: UTF-8 -*-
# Subinitial Test Automation Framework rigolds4000 series interface
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import logging

logger = logging.getLogger(__name__)

class RigolDS4000:
    vid = 0x1AB1
    pid = 0x04B1
    _mode_usb = "USB"
    _mode_dc = "DISCONNECTED"
    _mode_tcpip = "TCPIP"
    _retrycount = 3

    def __init__(self, serial=None):
        self.serial = serial
        self.mode = RigolDS4000._mode_dc
        self.idn = None
        self.ipaddr = None
        self.device = None
        """:type : usbtmc.Instrument"""

    def is_connected(self):
        return self.idn is not None

    def connect_usb(self):
        self.mode = RigolDS4000._mode_usb
        retries = 0
        import usbtmc
        while True:
            try:
                if self.serial is None:
                    self.device = usbtmc.Instrument(RigolDS4000.vid, RigolDS4000.pid)
                else:
                    self.device = usbtmc.Instrument(RigolDS4000.vid, RigolDS4000.pid, iSerial=self.serial)
                break
            except Exception as ex:
                retries += 1
                logger.info("connect failed on try {} with exception: {}".format(retries, ex))
                if retries > RigolDS4000._retrycount:
                    raise ex
        self.device.write(" ")
        self.idn = self._get_idn()

    def connect_tcpip(self, ipaddress):
        self.mode = RigolDS4000._mode_tcpip
        self.ipaddr = ipaddress
        import vxi11
        retries = 0
        while True:
            try:
                self.device = vxi11.Instrument(self.ipaddr)
                self.idn = self._get_idn()
                break
            except Exception as ex:
                retries += 1
                logger.info("connect failed on try {} with exception: {}".format(retries, ex))
                if retries > RigolDS4000._retrycount:
                    raise ex

    def _get_idn(self):
        retries = 0
        while True:
            try:
                return self.device.ask("*IDN?")
            except Exception as ex:
                retries += 1
                logger.info("communication for *IDN? failed on try {} with exception: {}".format(retries, ex))
                if retries > RigolDS4000._retrycount:
                    raise ex

    def close(self):
        self.idn = None
        import usb.util
        if self.mode == RigolDS4000._mode_usb and self.device is not None:
            self.device.reset()  # TODO: this method does nothing, find a way to reset the SCPI command interface
            usb.util.release_interface(self.device.device, self.device.iface)

    def __enter__(self):
        return self

    def __exit__(self):
        self.close()

    # DEVICE SPECIFIC CODE BELOW

    def run(self):
        self.device.write(":RUN")

    def stop(self):
        self.device.write(":STOP")

    def auto(self):
        self.device.write(":AUTO")

    def force_trigger(self):
        self.device.write(":FORCetrig")

    def hardcopy(self):
        self.device.write(":HARDcopy")

    CHANNEL_1 = "CHANnel1"
    CHANNEL_2 = "CHANnel2"
    CHANNEL_DIGITAL = "DIGital"
    CHANNEL_MATH = "MATH"
    CHANNEL_FFT = "FFT"

    def get_vpp(self, channel):
        return float(self.device.ask(":MEASure:VPP?"))

    def get_voffset(self, channel_idx):
        return float(self.device.ask(":CHANnel{}:OFFSet?".format(channel_idx)))

    def get_vscale(self, channel_idx):
        return float(self.device.ask(":CHANnel{}:SCALe?".format(channel_idx)))

    def get_waveform(self, channel_idx):
        self.device.write(":WAVeform:DATA? CHANnel{}".format(channel_idx))
        data = self.device.read_raw()
        offset = self.get_voffset(channel_idx)
        scale = self.get_vscale(channel_idx)

        return list(map(lambda x: ((int(x)-128)/255 - offset)*scale, data[10:]))
