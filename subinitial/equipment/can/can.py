from abc import ABC, abstractmethod
from typing import Any, Union, Dict, List
import traceback
from typing import Set, List, Union
import logging

import cantools.database as cantools


logger = logging.getLogger(__name__)


class CANFrame:
    def __init__(self, id: int, data: bytes, dlc: int|None=None, flags: int=0, timestamp: float|None=None):
        self.id = id
        self.data = data
        self.dlc = dlc if dlc is not None else len(data)
        self.flags = flags
        self.timestamp = timestamp

    def decode(self, dbc: cantools.Database) -> dict[str, float]:
        try:
            return dbc.decode_message(self.id, self.data) # type: ignore
        except Exception as ex:
            logger.debug('Failed to decode CAN Frame, reason: %s', ex)
            return {}

    @classmethod
    def encode(cls, message: cantools.Message, signals: dict[str, float], default_signal: float=0.0) -> 'CANFrame':
        all_signals = {sig.name: signals.get(sig.name, default_signal) for sig in message.signals}

        return cls(
            id=message.frame_id,
            data=message.encode(all_signals),
            # dlc=message.length,
            # flags=None,
            # timestamp=None
        )

    @classmethod
    def encode_bytes(cls, message: cantools.Message, encoded_bytes: bytes):
        return cls(
            id=message.frame_id,
            data=encoded_bytes,
            # dlc=message.length,
            # # flags=None,
            # # timestamp=None
        )

    def get_message_type(self, dbc: cantools.Database) -> Union[cantools.Message, None]:
        try:
            return dbc.get_message_by_frame_id(self.id)
        except:
            return None


class CANChannel(ABC):
    def __init__(self, device_id: Any, device_ch_num: int, name=''):
        self.name = name
        self.device_id = device_id
        self.device_ch_num = device_ch_num

    @abstractmethod
    def connect(self):
        pass

    @abstractmethod
    def disconnect(self):
        pass

    @abstractmethod
    def open_bus(self):
        pass

    @abstractmethod
    def close_bus(self):
        pass

    @abstractmethod
    def read(self, timeout) -> Union[CANFrame, None]:
        pass

    @abstractmethod
    def write(self, frame: CANFrame, timeout: float|None=None) -> None:
        pass
