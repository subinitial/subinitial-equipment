from typing import Union

from canlib import canlib, Frame

from .can import CANFrame, CANChannel


class KvaserCANChannel(CANChannel):
    def __init__(self, device_serial: int, device_ch_num: int, name='', bitrate=canlib.Bitrate.BITRATE_500K, data_bitrate=None, is_virtual=False, open_flags: canlib.Open=canlib.Open.NOFLAG):
        super().__init__(device_serial, device_ch_num, name)

        self.kvaser_bitrate = bitrate
        self.kvaser_data_bitrate = data_bitrate
        self.is_virtual = is_virtual
        self.open_flags = open_flags
        if self.is_virtual:
            self.open_flags = self.open_flags | canlib.Open.ACCEPT_VIRTUAL

        self.kvaser_channel: Union[canlib.Channel, None] = None

    @property
    def _virtual_ch_num(self):
        for virtual_ch in range(canlib.getNumberOfChannels()):
            virtual_ch_data = canlib.ChannelData(virtual_ch)
            if (virtual_ch_data.card_serial_no == self.device_id) and (virtual_ch_data.chan_no_on_card == (self.device_ch_num-1)):
                return virtual_ch
        
        return None

    def connect(self):
        self.kvaser_channel = canlib.openChannel(
            self._virtual_ch_num,
            self.open_flags,
            self.kvaser_bitrate,
            self.kvaser_data_bitrate)
        
    def open_bus(self):
        self.kvaser_channel.busOn()

    def close_bus(self):
        self.kvaser_channel.busOff()

    def disconnect(self):
        self.close_bus()
        self.kvaser_channel.close()
        self.kvaser_channel = None

    def read(self, timeout=100) -> Union[CANFrame, None]:
        try:
            frame = self.kvaser_channel.read(timeout)
            return CANFrame(frame.id, frame.data, frame.dlc, frame.flags, frame.timestamp)  
        except canlib.CanNoMsg:
            return None

    def write(self, frame: CANFrame, timeout: float|None=None) -> None:
        """ timeout in miliseconds, if no timeout is given the message is told to write without waiting to see if the write took place """

        if timeout is None:
            self.kvaser_channel.write(Frame(frame.id, frame.data, frame.dlc, frame.flags, frame.timestamp))
        else:
            if timeout > 0xFFFFFFFF:
                timeout =  0xFFFFFFFF
            self.kvaser_channel.writeWait(Frame(frame.id, frame.data, frame.dlc, frame.flags, frame.timestamp), timeout)
