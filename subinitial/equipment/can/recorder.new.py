from threading import Thread
from abc import ABC, abstractmethod

import can
import cantools.database as cantools

class RecorderOutput(ABC):
    @abstractmethod
    def record(self, msg: can.Message):
        pass


class BusDef:
    def __init__(self, name: str, bus: can.ThreadSafeBus, dbc: cantools.Database|None=None):
        self.name = name
        self.bus: can.Bus = bus # Stop the type checker from tripping on ThreadSafeBus (it can do everything a Bus can do)
        self.dbc = dbc


class CANRecorder:
    interface: str
    channel: str
    bitrate: int

    def __init__(self, buses: list[BusDef], outputs: list[RecorderOutput]=[]):
        self._buses = buses
        self.outputs: list[RecorderOutput] = outputs

        self.t_handles: list[Thread] = []
        self.t_keep_working = False

    def record_worker(self, bus: can.Bus):
        for msg in bus:
            for o in self.outputs: o.record(msg)

    def start(self):
        if len(self.t_handles) > 0:
            return

        self.t_keep_working = True
        self.t_handles = [Thread(target=lambda: self.record_worker(b.bus), daemon=True) for b in self._buses]
        for h in self.t_handles: h.start()

    def stop(self):
        if len(self.t_handles) == 0:
            return

        self.t_keep_working = False
        for h in self.t_handles: h.join()
        self.t_handles = []

if __name__ == "__main__":
    buses = [
        BusDef(
            name='Sample CAN Channel',
            bus=can.ThreadSafeBus(
                interface="kvaser",
                channel=1,
                can_filters = {}, # replaces whitelist
                bitrate=500000,
            ),
            dbc=None
        )
    ]

    listners = []
    c = CANRecorder(buses, listners)

    c.start()
    import time
    time.sleep(10)
    c.stop()