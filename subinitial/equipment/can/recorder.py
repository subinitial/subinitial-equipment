import csv
from pathlib import Path
import binascii
import datetime
from enum import Flag, auto
import json

import cantools.database as cantools

from .can import CANChannel, CANFrame


class CANChannelRecorder:
    '''Configurable CAN channel recorder'''
    class RecordLevel(Flag):
        NOTHING = auto() # Don't record anything, overrides all other flags
        ALL = auto() # Record every frame
        FOUND_IDS = auto() # Maintain a set of found IDs and the timestamp of each id's first appearance
        FOUND_ECUS = auto() # Maintain a set of found ECUs, with the timestamp and frame used to identify
        WHITELISTED_FRAMES = auto() # Record every frame whose ID is on a whitelist
        LATEST_FRAME_ONLY = auto() # Only keep the most recent frame of each ID

    class FoundID:
        CSV_HEADER = ["ID", "Timestamp Rel", "Timestamp Abs"]

        def __init__(self, id: int, frame_used: CANFrame):
            self.id = id
            self.frame_used = frame_used
            self.timestamp_abs = datetime.datetime.now().isoformat()

        def to_csv_dict(self):
            return {
                "ID": self.id,
                "Timestamp Rel": self.frame_used.timestamp/1000 if self.frame_used.timestamp is not None else '',
                "Timestamp Abs": self.timestamp_abs
            }

        def __hash__(self):
            return self.id.__hash__()

        def __eq__(self, other):
            return self.id.__eq__(other.id)


    class FoundUnrecognizedID:
        def __init__(self, id: int):
            self.id = id

        def __hash__(self):
            return self.id.__hash__()

        def __eq__(self, other):
            return self.id.__eq__(other.id)


    class FoundFrame:
        CSV_HEADER = ['ID', 'Data', 'DLC', 'Flags', 'Timestamp Rel', 'Timestamp Abs', 'Message name', 'Senders', 'Decoded data']
        
        def __init__(self, frame: CANFrame, dbc: cantools.Database=None):
            self.frame = frame
            self.timestamp_abs = datetime.datetime.now().isoformat()

            if dbc is None:
                self.message_name = ''
                self.senders = ''
                self.decoded_data = ''
            else:
                self.message_type = self.frame.get_message_type(dbc)
                if self.message_type is None:
                    self.message_name = 'Unrecognized Frame ID'
                    self.senders = ''
                    self.decoded_data = ''
                else:
                    self.message_name = self.message_type.name
                    self.senders = ', '.join(self.message_type.senders)
                    self.decoded_data = self.frame.decode(dbc) or 'ERROR could not decode data according to the DBC'

        def to_csv_dict(self):
            return {
                'ID': self.frame.id,
                'Data': binascii.hexlify(self.frame.data, '-'),
                'DLC': self.frame.dlc,
                'Flags': self.frame.flags,
                'Timestamp Rel': self.frame.timestamp/1000 if self.frame.timestamp is not None else '',
                'Timestamp Abs': self.timestamp_abs,
                'Message name': self.message_name,
                'Senders': self.senders,
                'Decoded data': json.dumps(self.decoded_data, indent=4) # PRETTY PRINT
            }

        def to_json_dict(self):
            return {
                'id': self.frame.id,
                'data': str(binascii.hexlify(self.frame.data, '-')),
                'dlc': self.frame.dlc,
                'flags': self.frame.flags,
                'timestamp_rel': self.frame.timestamp/1000 if self.frame.timestamp is not None else '',
                'timestamp_abs': self.timestamp_abs,
                'message_name': self.message_name,
                'senders': self.senders,
                'decoded_data': self.decoded_data
            }


    class FoundECU:
        CSV_HEADER = ["ECU", "ID", "Timestamp Rel", "Timestamp Abs"]

        def __init__(self, ecu_name: str, frame_used: CANFrame):
            self.ecu_name = ecu_name
            self.frame_used = frame_used
            self.timestamp_abs = datetime.datetime.now().isoformat()

        def to_csv_dict(self):
            return {
                "ECU": self.ecu_name,
                "ID Used": self.frame_used.id,
                "Timestamp Rel": self.frame_used.timestamp/1000 if self.frame_used.timestamp is not None else '',
                "Timestamp Abs": self.timestamp_abs
            }

        def __hash__(self):
            return self.ecu_name.__hash__()

        def __eq__(self, other):
            return self.ecu_name.__eq__(other)



    def __init__(self, channel: CANChannel, default_dbc: cantools.Database|None=None,
                 record_level: RecordLevel=RecordLevel.NOTHING, id_whitelist: Set[int]=set(),
                 write_to_disk=False, log_folder: Path=None, ecu_identifiers: Dict[int, str]={}):
        # Configuration
        self.channel = channel
        self.default_dbc = default_dbc
        self.record_level = record_level
        self.id_whitelist = id_whitelist
        
        # Data structures
        self.found_ids: Set[CANChannelRecorder.FoundID] = set()

        self.found_unrecognized_ids: Set[CANChannelRecorder.FoundUnrecognizedID] = set()

        self._frames_being_recorded = (self.RecordLevel.ALL in self.record_level) or (self.RecordLevel.WHITELISTED_FRAMES in self.record_level)
        self.found_frames_latest: Dict[int, CANChannelRecorder.FoundFrame] = {}
        self.found_frames: List[CANChannelRecorder.FoundFrame] = []
        self.frames_seen_count = 0

        self.ecu_identifiers = ecu_identifiers
        self.found_ecus: Set[CANChannelRecorder.FoundECU] = set()



        # Log to disk related properties
        self.write_to_disk = write_to_disk
        self.log_folder = log_folder
        self._header_written = False
        self._log_file = None
        self._log_writer: Union[None, csv.DictWriter] = None
        if self.write_to_disk:
            self.start_logging_to_disk()

    def start_logging_to_disk(self):
        if self.RecordLevel.NOTHING in self.record_level:
            return

        if self.log_folder is None:
            raise Exception('Cannot record to disk without a specified folder to write to')
        if not self.log_folder.exists():
            self.log_folder.mkdir(parents=True)
        log_file_path = Path(self.log_folder, f'{self.channel.name} - DEVICE {self.channel.device_id} - CH {self.channel.device_ch_num} - {datetime.datetime.now().strftime("%y-%m-%d %H_%M_%S")}.csv')

        self.write_to_disk = True

        csv_header = []
        if self._frames_being_recorded: # if frames and IDs being recorded, frames take priority for purposes of disk logging
            csv_header = CANChannelRecorder.FoundFrame.CSV_HEADER
        elif CANChannelRecorder.RecordLevel.FOUND_IDS in self.record_level:
            csv_header = CANChannelRecorder.FoundID.CSV_HEADER

        self._log_file = open(log_file_path, 'w', newline='', buffering=1)
        self._log_writer = csv.DictWriter(self._log_file, csv_header)

        if not self._header_written:
            self._log_writer.writeheader()

    def stop_logging_to_disk(self):
        # Dump latest frames to disk if appropriate
        if self.write_to_disk and (self.RecordLevel.LATEST_FRAME_ONLY in self.record_level) and (self.RecordLevel.ALL not in self.record_level):
            for found_frame in self.found_frames_latest.values():
                self._log_writer.writerow(found_frame.to_csv_dict())

        self.write_to_disk = False
        self._log_writer = None
        if self._log_file is not None:
            self._log_file.flush()
            self._log_file.close()
            self._log_file = None

    def record(self, frame: CANFrame):
        if self.RecordLevel.NOTHING in self.record_level:
            return

        # Frame recording
        if self._frames_being_recorded:
            self.frames_seen_count += 1

            frame_is_relevant = False
            if self.RecordLevel.ALL in self.record_level:
                frame_is_relevant = True
            elif self.RecordLevel.WHITELISTED_FRAMES in self.RecordLevel:
                if frame.id in self.id_whitelist:
                    frame_is_relevant = True

            if frame_is_relevant:
                found_frame = CANChannelRecorder.FoundFrame(frame, self.default_dbc)
                self.found_frames_latest[found_frame.frame.id] = found_frame

                if CANChannelRecorder.RecordLevel.LATEST_FRAME_ONLY not in self.record_level:
                    self.found_frames.append(found_frame)
                
                    if self.write_to_disk:
                        self._log_writer.writerow(found_frame.to_csv_dict())

        # ID recording
        if self.RecordLevel.FOUND_IDS in self.record_level:
            found_id = CANChannelRecorder.FoundID(frame.id, frame)
            
            if found_id not in self.found_ids:
                self.found_ids.add(found_id)

                if self.write_to_disk and not self._frames_being_recorded:
                    self._log_writer.writerow(found_id.to_csv_dict())

        # ECU recording
        if self.RecordLevel.FOUND_ECUS in self.record_level:
            ecu_name = self.ecu_identifiers.get(frame.id, None)
            if ecu_name is not None:
                found_ecu = self.FoundECU(ecu_name, frame)

                if found_ecu not in self.found_ecus:
                    self.found_ecus.add(found_ecu)

        # Misc checks
        if self.default_dbc is not None:
            if frame.get_message_type(self.default_dbc) is None:
                found_unrecognized_id = CANChannelRecorder.FoundUnrecognizedID(frame.id)
                if found_unrecognized_id not in self.found_unrecognized_ids:
                    self.found_unrecognized_ids.add(found_unrecognized_id)
