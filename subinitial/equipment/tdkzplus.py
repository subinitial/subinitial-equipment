# -*- coding: UTF-8 -*-
# Subinitial TDK-Lambda Z+ Series Power Supply Interface Code
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import logging
import socket
import time
from subinitial.equipment.utils import do_for

logger = logging.getLogger(__name__)


class _TdkZPlusBase:
    ID = "TDK-LAMBDA,Z"

    def __init__(self, com_port=None, com_baud=56700, host_ip=None, instrument_address=1, timeout=2, autoconnect=True):
        """Creates a TdkZPlus instance to remotely control the power supply.

        If neither a host_ip or com_port is provided then they must be provided when calling connect_serial or connect_lan

        :param com_port: serial communications port name
        :type com_port: string
        :param com_baud: serial baudrate
        :type com_baud: int
        :param host_ip: tcp/ip host as ip-address or Netbios hostname
        :type host_ip: string
        :param instrument_address: RS485 bus instrument address
        :type instrument_address: int
        :param timeout: device communication timeout in seconds (s)
        :type timeout: float
        :param autoconnect: if True and host_ip or com_port are provided, the device will be connected to at instantiation
        :type autoconnect: bool
        """
        self.instrument_address = 1
        self.host_ip = host_ip
        self.raw_socket_port = 8003
        self.com_port = com_port
        self.com_baud = 56700
        self.timeout = timeout
        self._buffer_size = 2048
        self.socket = None
        """:type: socket.socket"""
        self.conn = None
        """:type: """

        if (com_port or host_ip) and autoconnect:
            if self.host_ip:
                self.connect_lan()
            elif self.com_port:
                self.connect_serial()

    def connect_lan(self, host=None):
        """Connect to the device via LAN

        :param host: tcp/ip host as ip-address or Netbios hostname
        :type host: string
        """
        self.close()
        self.host_ip = host or self.host_ip
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(self.timeout)
        self.socket.connect((self.host_ip, self.raw_socket_port))
        self.select_instrument()
        self._test_connection()

    @staticmethod
    def list_serial_connections(_print=False):
        import os
        if os.name == 'nt':  # sys.platform == 'win32':
            from serial.tools.list_ports_windows import comports
        elif os.name == 'posix':
            from serial.tools.list_ports_posix import comports
        ports = list(sorted(comports()))
        if _print:
            print("=================================")
            print("LIST OF CONNECTED SERIAL DEVICES:")
            print("# : PORT - DESCRIPTION ; HWID")
            for i, (port, desc, hwid) in enumerate(ports):
                print(i, ":", port, "-", desc, ';', hwid)
            print("=================================")
        return ports

    def connect_serial(self, com_port=None, com_baud=None):
        """Connect to the device via serial (RS232 or USB-Serial)

        :param com_port: serial communications port name
        :type com_port: string
        :param com_baud: serial baudrate
        :type com_baud: int
        """
        import serial
        self.close()
        self.com_port = com_port or self.com_port
        self.com_baud = com_baud or self.com_baud
        try:
            self.conn = serial.Serial(self.com_port, self.com_baud, timeout=self.timeout)
        except Exception as ex:
            self._raise_connection_ex(ex)
        self.select_instrument()
        self._test_connection()

    def select_instrument(self, address=None):
        """Select which instrument address to communicate with on the RS485 bus

        :param address: rs485 instrument address
        :type address: int
        """
        self.instrument_address = address or self.instrument_address
        self._ask("INST:NSEL {}".format(self.instrument_address))

    def _raise_connection_ex(self, ex):
        # if self.com_port:
        #     self.list_serial_connections()
        raise ex

    def _test_connection(self):
        tries = 3
        while True:
            try:
                device_id = self._ask("*IDN?")
                if not device_id.startswith(self.ID):
                    self._raise_connection_ex(Exception("Invalid Device ID? response: {}, expected {}...".format(device_id, self.ID)))
                else:
                    break
            except Exception as ex:
                if tries:
                    tries -= 1
                    print("Invalid Device ID, retrying connection to {}".format(type(self).__name__))
                else:
                    self._raise_connection_ex(ex)
                    raise ex

    def _clear_socket(self):
        if not self.socket:
            return

        self.socket.settimeout(0.1)
        time.sleep(0.1)
        try:
            self.socket.recv(self._buffer_size)
        except:
            pass
        self.socket.settimeout(self.timeout)

    def _write(self, msgstr):
        """Send command to the device but don't wait for a response

        :param msgstr: message string
        :type msgstr: str
        """
        msgstr = msgstr.strip() + "\r\n"
        bmsg = msgstr.encode()
        if self.socket:
            self.socket.send(bmsg)
        elif self.conn:
            self.conn.write(bmsg)
        else:
            raise Exception("{} is not connected".format(type(self).__name__))

    def _ask(self, msgstr):
        """Send command to the device and read response

        :param msgstr: message string
        :type msgstr: str
        :return: response string
        :rtype: str
        """
        self._write(msgstr)
        if self.socket:
            response = self.socket.recv(self._buffer_size).decode()
        elif self.conn:
            response = self.conn.readline().decode()
        else:
            raise Exception("{} is not connected".format(type(self).__name__))
        return response.strip()

    def is_operation_complete(self):
        """Get operation status of the device

        ..note: this will most likely always return True as the device will not
                respond to it until it is finished completing all of its queued operations
        :return: True if all operations are complete and device is ready for next command
        :rtype: bool
        """
        opc = self._ask("*OPC?")
        return opc != "0"

    def reset(self, wait_for_response=True):
        """Reset the device to its default state"""
        self._write("*RST")
        if wait_for_response:
            return self.is_operation_complete()
        return 0

    def close(self):
        """Close the device connection"""
        if self.socket:
            self.socket.close()
        if self.conn:
            self.conn.close()

    def __enter__(self):
        return self

    def __exit__(self):
        self.close()


class TdkZPlus(_TdkZPlusBase):
    BLOCKING_CYLCEDELAY = 0.02  # how often to check to see if output state matches commanded state

    def _threshold(self, setpoint, measurement):
        delta = max(0.05 * abs(setpoint), 100e-3)
        # print('delta', delta, setpoint, measurement)
        return (setpoint - delta) <= measurement <= (setpoint + delta)

    def _check_status(self, channel, vcommand, icommand):
        if vcommand is not None:
            vmeas = self.get_voltage_measured(channel)
            if not self._threshold(vcommand, vmeas):
                return False
        if icommand is not None:
            imeas = self.get_current_measured(channel)
            if not self._threshold(icommand, imeas):
                return False
        return True

    def set_voltage(self, channel=None, commanded=None, timeout=5.0):
        """ Set the commanded voltage setpoint of power supply.

        ..note: if only one parameter is provided it will be treated as _commanded_

        :param channel: parameter unused but included for compatibility with other drivers
        :param commanded: commanded voltage setpoint in Volts (V)
        :type commanded: float
        :param timeout: optional time to wait for measured current to settle in seconds (s)
        :type timeout: float
        """
        if commanded is None and channel is not None:
            commanded = channel
            channel = None

        self._write("SOURCE:VOLTAGE {}".format(commanded))
        if timeout and self.get_output(channel):
            timeout = max(0.5, timeout)
            if not do_for(timeout, self._check_status,
                          (channel, commanded, None),
                          repeatdelay=self.BLOCKING_CYLCEDELAY):
                raise Exception(
                    "{} commanded voltage {}V not reached within {}s".format(type(self).__name__, commanded, timeout))

    def set_current(self, channel=None, commanded=None, timeout=None):
        """ Set the commanded current setpoint of power supply.

        ..note: if only one parameter is provided it will be treated as _commanded_

        :param channel: parameter unused but included for compatibility with other drivers
        :param commanded: commanded current setpoint in Amps (A)
        :type commanded: float
        :param timeout: optional time to wait for measured current to settle in seconds (s)
        :type timeout: float
        """
        if commanded is None and channel is not None:
            commanded = channel
            channel = None

        self._write("SOURCE:CURRENT {}".format(commanded))
        if timeout and self.get_output(channel):
            timeout = max(0.5, timeout)
            if not do_for(timeout, self._check_status,
                          (channel, None, commanded),
                          repeatdelay=self.BLOCKING_CYLCEDELAY):
                raise Exception(
                    "{} commanded current {}A not reached within {}s".format(type(self).__name__, commanded, timeout))

    def output_enable(self, channel=None, timeout=5.0, wait_voltage_not_current=True):
        """Enable the output of the power supply

        :param channel: parameter unused but included for compatibility with other drivers
        :param timeout: maximum time to wait for setpoint to settle in seconds (s)
        :type timeout: float
        :param wait_voltage_not_current: if True, wait up to timeout seconds for voltage setpoint to settle, if False wait
            up to timeout seconds for current setpoint to settle
        :type wait_voltage_not_current: bool
        """

        # FIXES SORENSEN FIRMWARE BUG: glitching on output if set_voltage is called immediately before output_enable
        # time.sleep(0.025)

        self._write("OUTPUT:STATE {}".format(1))
        if timeout:
            timeout = max(0.5, timeout)
            if wait_voltage_not_current:
                cmd = self.get_voltage_command(channel)
                args = channel, cmd, None
            else:
                cmd = self.get_current_command(channel)
                args = channel, None, cmd
            if not do_for(timeout, self._check_status,
                          args,
                          repeatdelay=self.BLOCKING_CYLCEDELAY):
                raise Exception("{} commanded {} {} not reached within {}s".format(
                    type(self).__name__, ["current", "voltage"][wait_voltage_not_current], cmd, timeout))
        # else:
            # FIXES SORENSEN FIRMWARE BUG: glitching on output if set_voltage is called immediately before output_enable
            # time.sleep(0.025)

    def output_disable(self, channel=None, delay=0.100):
        """disable the output of the powersupply

        :param channel: parameter unused but included for compatibility with other drivers
        :param delay: time in seconds (s)
        :type delay: float
        """
        self._write("OUTPUT:STATE {}".format(0))
        time.sleep(delay)

    def get_output(self, channel=None):
        """
        Identifies if a channel is enabled or not
        :param channel: parameter unused but included for compatibility with other drivers
        :return: channel status. True is on, False is off
        :rtype: bool
        """
        return bool(int(self._ask("OUTPUT:STATE?")))

    def set_ovp(self, ovp_setpoint):
        """Set over-voltage protection, must be at least 5% higher than the current voltage command

        :param ovp_setpoint: voltage in Volts (V)
        :type ovp_setpoint: float
        """
        self._write("SOURCE:VOLTAGE:PROTECTION:LEVEL {}".format(ovp_setpoint))

    def get_ovp(self):
        """Get over-voltage protection setpoint

        :return: Voltage in Volts (V)
        :rtype: float
        """
        ovp = self._ask("SOURCE:VOLTAGE:PROTECTION:LEVEL?")
        # print(ovp)
        return float(ovp)

    def get_voltage_command(self, channel=None):
        """ Get the voltage setpoint

        :param channel: parameter unused but included for compatibility with other drivers
        :return: Voltage in Volts (V)
        :rtype: float
        """
        return float(self._ask("SOURCE:VOLTAGE?"))

    def get_current_command(self, channel=None):
        """ Get the current setpoint

        :param channel: parameter unused but included for compatibility with other drivers
        :return: Current in Amps (A)
        :rtype: float
        """
        return float(self._ask("SOURCE:CURRENT?"))

    def get_voltage_measured(self, channel=None):
        """ Get the measured voltage at the power supply output

        :param channel: parameter unused but included for compatibility with other drivers
        :return: Voltage in Volts (V)
        :rtype: float
        """
        return float(self._ask("MEASURE:VOLTAGE?"))

    def get_current_measured(self, channel=None):
        """ Get the measured current sourced by the power supply

        :param channel: parameter unused but included for compatibility with other drivers
        :return: Current in Amps (A)
        :rtype: float
        """
        return float(self._ask("MEASURE:CURRENT?"))

    def get_power_measured(self, channel=None):
        """ Get the measured power sourced by the power supply

        :param channel: parameter unused but included for compatibility with other drivers
        :return: Power in Watts (W)
        :rtype: float
        """
        return float(self._ask("MEASURE:POWER?"))

    def is_limiting(self, channel=None):
        """Get channel v-limiting and i-limiting state

        :param channel: parameter unused but included for compatibility with other drivers
        :return: voltage is limiting, current is limiting
        :rtype: bool, bool
        """
        cccv = self._ask("OUTPUT:MODE?")
        return cccv == "CV", cccv == "CC"

    def output(self, channel=None, voltage=None, current=None, enable=True, settling_time=0.1):
        """Basic function to turn on the power supply channel with a set voltage and current.
        Waits for power supply voltage to come up, then waits an extra 'settling_time' for general settling.

        :param channel: parameter unused but included for compatibility with other drivers
        :param voltage: voltage setpoint, if None setpoint will not be changed
        :type voltage: float
        :param voltage: current setpoint, if None setpoint will not be changed
        :type current: float
        :param enable: if True enables the powersupply output, if false disables the powersupply output
        :type enable: bool
        :param settling_time: seconds (s) time to wait after voltage, current, and output state have been set
        :type settling_time: float
        """
        if voltage is not None:
            self.set_voltage(channel=channel, commanded=voltage)
        if current is not None:
            self.set_current(channel=channel, commanded=current)
        if enable:
            self.output_enable(channel=channel)
        else:
            self.output_disable(channel=channel)
        time.sleep(settling_time)  # Wait for all voltages & currents to stabilize
