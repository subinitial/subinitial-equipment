import sys
import inspect
import os
from .version import VERSION
VERSION_EQUIPMENT = VERSION

# adding "lib" directory as a fallback 3rd party library support
_execfile = inspect.stack()[0][1]
_pathentry = os.path.join(os.path.dirname(_execfile), "lib")
sys.path.append(_pathentry)
