# -*- coding: UTF-8 -*-
# Subinitial Test Automation Framework rigolds1000z series interface
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import logging
from .utils import open_stubbornly, do_for

logger = logging.getLogger(__name__)


def _sanitize_chan(chan):
    if type(chan) == int or type(chan) == float:
        return "CHANnel{}".format(chan)
    return chan

class RigolDS1000Z:
    vid = 0x1AB1
    pid = 0x0588
    _mode_usb = "USB"
    _mode_dc = "DISCONNECTED"
    _mode_tcpip = "TCPIP"
    _retrycount = 3

    def __init__(self, serial=None):
        self.serial = serial
        self.mode = RigolDS1000Z._mode_dc
        self.idn = None
        self.ipaddr = None
        self.device = None
        """:type : usbtmc.Instrument"""

        self.aquire = self.Aquire()
        self.trigger = self.Trigger()
        self.math = self.Math()
        self.waveform = self.Waveform()
        self.timebase = self.Timebase()
        self.channels = self.Channels()
        self.measure = self.Measure()
        self._subsections = [
            self.aquire, self.trigger, self.math, self.waveform, self.timebase, self.channels,
            self.measure,
        ]

    def is_connected(self):
        return self.idn is not None

    def connect_usb(self):
        self.mode = RigolDS1000Z._mode_usb
        retries = 0
        import usbtmc
        while True:
            try:
                if self.serial is None:
                    self.device = usbtmc.Instrument(RigolDS1000Z.vid, RigolDS1000Z.pid)
                else:
                    self.device = usbtmc.Instrument(RigolDS1000Z.vid, RigolDS1000Z.pid, iSerial=self.serial)
                for sub in self._subsections:
                    sub.device = self
                break
            except Exception as ex:
                retries += 1
                logger.info("connect failed on try {} with exception: {}".format(retries, ex))
                if retries > RigolDS1000Z._retrycount:
                    raise ex
        self.device.write(" ")
        self.idn = self._get_idn()

    def connect_tcpip(self, ipaddress):
        self.mode = RigolDS1000Z._mode_tcpip
        self.ipaddr = ipaddress
        import vxi11
        retries = 0
        while True:
            try:
                self.device = vxi11.Instrument(self.ipaddr)
                self.idn = self._get_idn()
                for sub in self._subsections:
                    sub.device = self
                break
            except Exception as ex:
                retries += 1
                logger.info("connect failed on try {} with exception: {}".format(retries, ex))
                if retries > RigolDS1000Z._retrycount:
                    raise ex

    def _get_idn(self):
        retries = 0
        while True:
            try:
                return self.device.ask("*IDN?")
            except Exception as ex:
                retries += 1
                logger.info("communication for *IDN? failed on try {} with exception: {}".format(retries, ex))
                if retries > RigolDS1000Z._retrycount:
                    raise ex

    def close(self):
        self.idn = None
        import usb.util
        if self.mode == self._mode_usb and self.device is not None:
            self.device.reset()  # TODO: this method does nothing, find a way to reset the SCPI command interface
            usb.util.release_interface(self.device.device, self.device.iface)

    def __enter__(self):
        return self

    def __exit__(self):
        self.close()

    def _write(self, *args):
        retries = 2
        while True:
            try:
                return self.device.write(*args)
            except Exception as ex:
                if retries:
                    retries -= 1
                    print("{} communication error, retrying...".format(type(self)))
                else:
                    raise ex

    def _ask(self, *args):
        retries = 2
        while True:
            try:
                return self.device.ask(*args)
            except Exception as ex:
                if retries:
                    retries -= 1
                    print("{} communication error, retrying...".format(type(self)))
                else:
                    raise ex

    def _read_raw(self):
        retries = 2
        while True:
            try:
                return self.device.read_raw()
            except Exception as ex:
                if retries:
                    retries -= 1
                    print("{} communication error, retrying...".format(type(self)))
                else:
                    raise ex

    def reset(self, wait_for_response=False):
        self._write("*RST")
        if wait_for_response:
            return self.is_operation_complete()
        return 0

    # DEVICE SPECIFIC CODE BELOW
    def run(self):
        self._write(":RUN")
        return self.is_operation_complete()

    def stop(self):
        self._write(":STOP")
        return self.is_operation_complete()

    def auto(self):
        self._write(":AUToscale")
        return self.is_operation_complete()

    def clear(self):
        self._write(":CLEar")
        return self.is_operation_complete()

    def single(self):
        self._write(":SINGle")
        return self.is_operation_complete()

    def force_trigger(self):
        self._write(":TFORce")
        return self.is_operation_complete()

    def is_operation_complete(self):
        return self._ask("*OPC?") != "0"

    def _waitfn(self, expected):
        stat = self.trigger.get_status()
        return stat == expected, stat

    def wait_for_status(self, expected_status, timeout=3.0, raise_timeout_exception=True):
        matched, stat = do_for(timeout, self._waitfn, (expected_status,))
        if not matched and raise_timeout_exception:
            raise TimeoutError("Scope status stayed {} and did not reach {}".format(stat, expected_status))
        return matched

    CHANNEL_1 = "CHANnel1"
    CHANNEL_2 = "CHANnel2"
    CHANNEL_3 = "CHANnel3"
    CHANNEL_4 = "CHANnel4"
    CHANNEL_DIGITAL = "DIGital"
    CHANNEL_MATH = "MATH"
    CHANNEL_FFT = "FFT"

    def get_vpp(self, channel):
        return float(self._ask(":MEASure:VPP?"))

    def get_voffset(self, channel_idx):
        return float(self._ask(":CHANnel{}:OFFSet?".format(channel_idx)))

    def get_vscale(self, channel_idx):
        return float(self._ask(":CHANnel{}:SCALe?".format(channel_idx)))

    def get_waveform(self, channel_idx):
        self._write(":WAVeform:DATA? CHANnel{}".format(channel_idx))
        data = self._read_raw()
        offset = self.get_voffset(channel_idx)
        scale = self.get_vscale(channel_idx)

        return list(map(lambda x: ((int(x)-128)/255 - offset)*scale, data[10:]))

    # ON = "ON"
    # OFF = "OFF"
    # FORMAT_BMP24 = "BMP24"
    # FORMAT_BMP8 = "BMP8"
    # FORMAT_PNG = "PNG"
    # FORMAT_JPEG = "JPEG"
    # FORMAT_TIFF = "TIFF"

    def save_image(self, fpath):  #, format=FORMAT_JPEG, color=ON, invert=1):
        self._write(":DISP:DATA?") #.format(color, invert, format))
        # self._write(":DISPlay:DATA?")
        data = self._read_raw()
        hdrlen = data[1]-ord('0')+2
        msglen = int(data[2:hdrlen])
        with open_stubbornly(fpath, mode="wb") as f:
            f.write(data[hdrlen:hdrlen+msglen])

    GRADING_TIME_MIN = "MIN"
    GRADING_TIME_0P1 = "0.1"
    GRADING_TIME_0P2 = "0.2"
    GRADING_TIME_0P5 = "0.5"
    GRADING_TIME_1 = "1"
    GRADING_TIME_5 = "5"
    GRADING_TIME_10 = "10"
    GRADING_TIME_INFINITE = "INF"

    def set_grading_time(self, grading_time):
        self._write(":DISPlay:GRADing:TIME {}".format(grading_time))

    def get_grading_time(self):
        self._ask(":DISPlay:GRADing:TIME?")

    class Channels:
        def __init__(self):
            self.device = None

        BWLIMIT_OFF = "OFF"
        BWLIMIT_20MHZ = "20M"

        def set_bwlimit(self, channel, bwlimit):
            self.device._write(":CHANnel{}:BWLimit {}".format(channel, bwlimit))

        def get_bwlimit(self, channel):
            self.device._ask(":CHANnel{}:BWLimit?".format(channel))

        COUPLING_AC = "AC"
        COUPLING_DC = "DC"
        COUPLING_GND = "GND"

        def set_coupling(self, channel, coupling):
            self.device._write(":CHANnel{}:COUPling {}".format(channel, coupling))

        def get_coupling(self, channel):
            self.device._ask(":CHANnel{}:COUPling?".format(channel))

        DISPLAY_ON = "ON"
        DISPLAY_OFF = "OFF"

        def set_display(self, channel, display):
            self.device._write(":CHANnel{}:DISPlay {}".format(channel, display))

        def get_display(self, channel):
            self.device._ask(":CHANnel{}:DISPlay?".format(channel))

        INVERT_ON = "ON"
        INVERT_OFF = "OFF"

        def set_invert(self, channel, invert):
            self.device._write(":CHANnel{}:INVert {}".format(channel, invert))

        def get_invert(self, channel):
            self.device._ask(":CHANnel{}:INVert?".format(channel))

        def set_offset(self, channel, offset):
            self.device._write(":CHANnel{}:OFFSet {}".format(channel, offset))

        def get_offset(self, channel):
            self.device._ask(":CHANnel{}:OFFSet?".format(channel))

        def set_range(self, channel, range):
            self.device._write(":CHANnel{}:RANGe {}".format(channel, range))

        def get_range(self, channel):
            self.device._ask(":CHANnel{}:RANGe?".format(channel))

        def set_scale(self, channel, scale):
            self.device._write(":CHANnel{}:SCALe {}".format(channel, scale))

        def get_scale(self, channel):
            self.device._ask(":CHANnel{}:SCALe?".format(channel))

        PROBE_0P01 = "0.01"
        PROBE_0P02 = "0.02"
        PROBE_0P05 = "0.05"
        PROBE_0P1 = "0.1"
        PROBE_0P2 = "0.2"
        PROBE_0P5 = "0.5"
        PROBE_1 = "1"
        PROBE_2 = "2"
        PROBE_5 = "5"
        PROBE_10 = "10"
        PROBE_20 = "20"
        PROBE_50 = "50"
        PROBE_100 = "100"
        PROBE_200 = "200"
        PROBE_500 = "500"
        PROBE_1000 = "1000"

        def set_probe(self, channel, probe):
            self.device._write(":CHANnel{}:PROBe {}".format(channel, probe))

        def get_probe(self, channel):
            self.device._ask(":CHANnel{}:PROBe?".format(channel))

        UNITS_VOLTS = "VOLT"
        UNITS_WATTS = "WATT"
        UNITS_AMPS = "AMP"
        UNITS_UNKNOWN = "UNKN"

        def set_units(self, channel, units):
            self.device._write(":CHANnel{}:UNITs {}".format(channel, units))

        def get_units(self, channel):
            self.device._ask(":CHANnel{}:UNITs?".format(channel))

    class Aquire:
        def __init__(self):
            self.device = None

        TYPE_NORMAL = "NORMAL"
        TYPE_AVERAGE = "AVERAGE"
        TYPE_PEAKDETECT = "PEAKDETECT"
        TYPE_HIGHRESOLUTION = "HRESOLUTION"

        def get_type(self):
            return self.device._ask(":ACQuire:TYPE?")

        def set_type(self, type_):
            self.device._write(":ACQuire:TYPE " + type_)

        def get_samplerate(self):
            return self.device._ask(":ACQuire:SRATe?")

        MEMORYDEPTH_AUTO = "AUTO"

        def get_memorydepth(self):
            return self.device._ask(":ACQuire:MDEPth?")

        def set_memorydepth(self, memdep):
            self.device._write(":ACQuire:MDEPth {}".format(memdep))

        AVERAGING_2 = 2
        AVERAGING_4 = 4
        AVERAGING_8 = 8
        AVERAGING_16 = 16
        AVERAGING_32 = 32
        AVERAGING_64 = 64
        AVERAGING_128 = 128
        AVERAGING_256 = 256
        AVERAGING_512 = 512
        AVERAGING_1024 = 1024

        def get_averaging(self):
            return int(self.device._ask(":ACQuire:AVER?"))

        def set_averaging(self, averaging):
            self.device._write(":ACQuire:AVER " + averaging)

    class Timebase:
        def __init__(self):
            self.device = None

        MODE_MAIN = "MAIN"
        MODE_XY = "XY"
        MODE_ROLL = "ROLL"

        def set_mode(self, mode):
            self.device._write(":TIMebase:MAIN:MODE {}".format(mode))

        def get_mode(self):
            return self.device._ask(":TIMebase:MAIN:MODE?")

        def set_offset(self, offset):
            self.device._write(":TIMebase:MAIN:OFFSet {}".format(offset))

        def get_offset(self):
            return float(self.device._ask(":TIMebase:MAIN:OFFSet?"))

        def set_scale(self, scale):
            self.device._write(":TIMebase:MAIN:SCALe {}".format(scale))

        def get_scale(self):
            return float(self.device._ask(":TIMebase:MAIN:SCALe?"))

        def set_delay(self, on_notoff, offset=0, scale=500e-9):
            self.device._write(":TIMebase:DELay:ENABle {}".format(int(on_notoff)))
            if on_notoff:
                self.device._write(":TIMebase:DELay:OFFSet {}".format(offset))
                self.device._write(":TIMebase:DELay:SCALe {}".format(scale))

        def get_delay(self):
            on_notoff = self.device._ask(":TIMebase:DELay:ENABle?")
            offset = float(self.device._ask(":TIMebase:DELay:OFFSet?"))
            scale = float(self.device._ask(":TIMebase:DELay:SCALe?"))
            return on_notoff, offset, scale

    class Trigger:
        NO_CHANGE = None

        def __init__(self):
            self.device = None
            self.mode = self.MODE_EDGE

        MODE_EDGE = "EDGE"
        MODE_PULSE = "PULSE"
        MODE_RUNT = "RUNT"
        MODE_WIND = "WIND"
        MODE_NEDG = "NEDG"
        MODE_VIDEO = "VID"
        MODE_SLOPE = "SLOP"
        MODE_PATTERN = "PATTERN"
        MODE_DELAY = "DEL"
        MODE_TIMEOUT = "TIM"
        MODE_DURATION = "DUR"
        MODE_SHOLD = "SHOL"

        def get_mode(self):
            """gets trigger mode

            :rtype: string
            """
            self.mode = self.device._ask(":TRIG:MODE?")
            return self.mode

        def set_mode(self, mode):
            """sets trigger mode

            :param mode: refer to Trigger.MODE_* options
            """
            self.mode = mode
            self.device._write(":TRIG:MODE " + mode)

        SOURCE_CHANNEL1 = "CHANnel1"
        SOURCE_CHANNEL2 = "CHANnel2"
        SOURCE_CHANNEL3 = "CHANnel3"
        SOURCE_CHANNEL4 = "CHANnel4"
        SOURCE_EXT = "EXT"
        SOURCE_ACLINE = "ACLine"

        def get_source(self):
            return self.device._ask("TRIGger:{}:SOURce?".format(self.mode))

        def set_source(self, source):
            self.device._write("TRIGger:{}:SOURce {}".format(self.mode, _sanitize_chan(source)))

        def get_level(self):
            return self.device._ask("TRIGger:{}:LEVel?".format(self.mode))

        def set_level(self, level):
            """set trigger level (DEPRECATED)

            :param level: trigger level from -6.0 to 6.0 of Scale (V/div)
            :type level: float
            """
            self.device._write("TRIGger:{}:LEVel {}".format(self.mode, level))

        SLOPE_POSITIVE = "POSITIVE"
        SLOPE_NEGATIVE = "NEGATIVE"

        def set_edge(self, source=NO_CHANGE, slope=NO_CHANGE, level=NO_CHANGE):
            """Set the edge trigger parameters

            :param source: refer to Trigger.SOURCE_* options
            :param slope: POSITIVE or NEGATIVE slope, refer to Trigger.SLOPE_* options
            :param level: trigger level from -6.0 to 6.0 of Scale (V/div)
            :type level: float
            """
            if source is not self.NO_CHANGE:
                self.device._write(":TRIGger:EDGE:SOURce {}".format(_sanitize_chan(source)))
            if slope is not self.NO_CHANGE:
                self.device._write(":TRIGger:EDGE:SLOPe {}".format(slope))
            if level is not self.NO_CHANGE:
                self.device._write(":TRIGger:EDGE:LEVel {}".format(level))

        def get_edge(self):
            src = self.device._ask(":TRIGger:EDGE:SOURce?")
            slope = self.device._ask(":TRIGger:EDGE:SLOPe?")
            level = float(self.device._ask(":TRIGger:EDGE:LEVel?"))
            return src, slope, level

        DELAYTYPE_GREATER = "GRE"
        DELAYTYPE_LESS = "LESS"
        DELAYTYPE_GLESS = "GLES"
        DELAYTYPE_GOUT = "GOUT"

        def set_delay(self, source_a=NO_CHANGE, slope_a=NO_CHANGE, source_b=NO_CHANGE, slope_b=NO_CHANGE,
                      delaytype=NO_CHANGE, t_upper=NO_CHANGE, t_lower=NO_CHANGE):
            if source_a is not self.NO_CHANGE:
                self.device._write(":TRIGger:DELay:SA {}".format(_sanitize_chan(source_a)))
            if slope_a is not self.NO_CHANGE:
                self.device._write(":TRIGger:DELay:SLOPA {}".format(slope_a))
            if source_b is not self.NO_CHANGE:
                self.device._write(":TRIGger:DELay:SB {}".format(_sanitize_chan(source_b)))
            if slope_b is not self.NO_CHANGE:
                self.device._write(":TRIGger:DELay:SLOPB {}".format(slope_b))
            if delaytype is not self.NO_CHANGE:
                self.device._write(":TRIGger:DELay:TYPe {}".format(delaytype))
            if t_upper is not self.NO_CHANGE:
                self.device._write(":TRIGger:DELay:TUPPer {}".format(t_upper))
            if t_lower is not self.NO_CHANGE:
                self.device._write(":TRIGger:DELay:TLOWer {}".format(t_lower))

        def get_delay(self):
            src_a = self.device._ask(":TRIGger:DELay:SA?")
            slope_a = self.device._ask(":TRIGger:DELay:SLOPA?")
            src_b = self.device._ask(":TRIGger:DELay:SB?")
            slope_b = self.device._ask(":TRIGger:DELay:SLOPB?")
            delaytype = self.device._ask(":TRIGger:DELay:TYPe?")
            t_upper = float(self.device._ask(":TRIGger:DELay:TUPPer?"))
            t_lower = float(self.device._ask(":TRIGger:DELay:TLOWer?"))
            return src_a, slope_a, src_b, slope_b, delaytype, t_upper, t_lower

        SWEEP_AUTO = "AUTO"
        SWEEP_NORMAL = "NORMAL"
        SWEEP_SINGLE = "SINGLE"

        def get_sweep(self):
            return self.device._ask(":TRIGger:SWEep?")

        def set_sweep(self, sweep):
            self.device._write(":TRIGger:SWEep {}".format(sweep))

        COUPLING_DC = "DC"
        COUPLING_AC = "AC"
        COUPLING_HFREJECT = "HF"
        COUPLING_LFREJECT = "LF"

        def get_coupling(self):
            return self.device._ask(":TRIGger:COUPling?")

        def set_coupling(self, sweep):
            self.device._write(":TRIGger:COUPling {}".format(sweep))

        def get_holdoff(self):
            return self.device._ask(":TRIGger:HOLDoff?")

        def set_holdoff(self, holdoff):
            """set trigger holdoff time

            :param holdoff: holdoff time from 16e-9 to 10 seconds
            :type holdoff: float
            """
            self.device._write(":TRIGger:HOLDoff {}".format(holdoff))

        STATUS_RUN = "RUN"
        STATUS_STOP = "STOP"
        STATUS_TRIGGERED = "T'D"
        STATUS_WAIT = "WAIT"
        STATUS_AUTO = "AUTO"

        def get_status(self):
            return self.device._ask(":TRIGger:STATus?")



    class Math:
        def __init__(self):
            self.device = None

        DISPLAY_ON = "ON"
        DISPLAY_OFF = "OFF"

        def get_display(self):
            return self.device._ask(":MATH:DISPlay?")

        def set_display(self, display):
            self.device._write(":MATH:DISPlay " + display)

        OPERATION_A_PLUS_B = "A+B"
        OPERATION_A_MINUS_B = "A-B"
        OPERATION_A_TIMES_B = "A*B"
        OPERATION_FFT = "FFT"

        def get_operation(self):
            return self.device._ask(":MATH:OPERate?")

        def set_operation(self, operation):
            self.device._write(":MATH:OPERate " + operation)

        def get_fft_display(self):
            return self.device._ask(":FFT:DISPlay?")

        def set_fft_display(self, display):
            self.device._write(":FFT:DISPlay " + display)

    class Measure:
        def __init__(self):
            self.device = None

        def clear_all(self):
            self.device._write("MEASure:CLEar ALL")

        CHANNEL1 = "CHANnel1"
        CHANNEL2 = "CHANnel2"
        CHANNEL3 = "CHANnel3"
        CHANNEL4 = "CHANnel4"

        def get_rising_delay(self, achan, bchan):
            return float(self.device._ask(":MEASure:ITEM? RDELay,{},{}".format(
                _sanitize_chan(achan),
                _sanitize_chan(bchan))))

        def get_falling_delay(self, achan, bchan):
            return float(self.device._ask(":MEASure:ITEM? FDELay,{},{}".format(
                _sanitize_chan(achan),
                _sanitize_chan(bchan))))

        def get_risetime(self, chan):
            return float(self.device._ask(":MEASure:ITEM? RTIMe,{}".format(_sanitize_chan(chan))))

        def get_falltime(self, chan):
            return float(self.device._ask(":MEASure:ITEM? FTIMe,{}".format(_sanitize_chan(chan))))


    class Waveform:
        NO_CHANGE = None
        def __init__(self):
            self.device = None
            self.format = self.FORMAT_BYTE
            self.last_preamble = None
            """:type: RigolDS1000Z.Waveform.Preamble"""

        SOURCE_CHANNEL1 = "CHANNEL1"
        SOURCE_CHANNEL2 = "CHANNEL2"
        SOURCE_CHANNEL3 = "CHANNEL3"
        SOURCE_CHANNEL4 = "CHANNEL4"
        SOURCE_MATH = "MATH"

        def get_source(self):
            return self.device._ask(":WAVeform:SOURce?")

        def set_source(self, source):
            self.device._write(":WAVeform:SOURce " + _sanitize_chan(source))

        MODE_NORMAL = "NORMal"
        MODE_MAXIMUM = "MAXimum"
        MODE_RAW = "RAW"

        def get_mode(self):
            return self.device._ask(":WAVeform:MODE?")

        def set_mode(self, mode):
            self.device._write(":WAVeform:MODE " + mode)

        FORMAT_WORD = "WORD"
        FORMAT_BYTE = "BYTE"
        FORMAT_ASCII = "ASCii"

        def get_format(self):
            return self.device._ask(":WAVeform:FORMat?")

        def set_format(self, fmt):
            self.device._write(":WAVeform:FORMat {}".format(fmt))

        def get_data(self):
            self.device._write(":WAVeform:DATA?")
            return self.device._read_raw()

        def set_start(self, start):
            self.device._write(":WAVeform:STARt {}".format(start))

        def get_start(self):
            return int(self.device._ask(":WAVeform:STARt?"))

        def set_stop(self, stop):
            self.device._write(":WAVeform:STOP {}".format(stop))

        def get_stop(self):
            return int(self.device._ask(":WAVeform:STOP?"))


        def get_xincrement(self):
            return float(self.device._ask(":WAVeform:XINCrement?"))

        def get_xreference(self):
            return float(self.device._ask(":WAVeform:XREFerence?"))

        def get_xorigin(self):
            return float(self.device._ask(":WAVeform:XORigin?"))

        def get_yincrement(self):
            return float(self.device._ask(":WAVeform:YINCrement?"))

        def get_yreference(self):
            return float(self.device._ask(":WAVeform:YREFerence?"))

        def get_yorigin(self):
            return float(self.device._ask(":WAVeform:YORigin?"))

        class Preamble:
            def __init__(self, fmt, type, points, count, xincrement,
                         xorigin, xreference, yincrement, yorigin, yreference):
                self.fmt = int(fmt)  # ["BYTE", "WORD", "ASCII"]
                self.type = int(type)  # ["NORMAL", "MAXIMUM", "RAW"]
                self.points = int(points)
                self.count = int(count)
                self.xincrement = float(xincrement)
                self.xorigin = float(xorigin)
                self.xreference = float(xreference)
                self.yincrement = float(yincrement)
                self.yorigin = float(yorigin)
                self.yreference = float(yreference)

        def get_preamble(self):
            preamble = self.device._ask(":WAVeform:PREamble?")
            return self.Preamble(*preamble.split(","))

        def samples(self):
            """Samples as an interable

            :return: returns samples as iterable
            :rtype: iter(int)
            """
            pre = self.last_preamble = self.get_preamble()
            if pre.fmt != 0:
                self.set_format(self.FORMAT_BYTE)
            points_per_req = [250000, 125000, 15625][pre.type]

            idx = 1
            points_remain = pre.points
            while points_remain >= points_per_req:
                self.set_start(idx)
                self.set_stop(idx + points_per_req - 1)
                points_remain -= points_per_req
                data = self.get_data()

                for y in data[11:-1]:
                    x = (idx - 1 - (pre.xorigin + pre.xreference)) * pre.xincrement
                    idx += 1
                    y = (y - (pre.yorigin + pre.yreference)) * pre.yincrement
                    yield x, y

            if points_remain:
                self.set_start(idx)
                self.set_stop(idx + points_remain - 1)
                data = self.get_data()

                for y in data[11:-1]:
                    x = (pre.xorigin + pre.xreference) + (idx - 1) * pre.xincrement
                    idx += 1
                    y = (y - (pre.yorigin + pre.yreference)) * pre.yincrement
                    yield x, y
