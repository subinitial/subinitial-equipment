# -*- coding: UTF-8 -*-
# Subinitial Test Automation Framework message router client for accessing shared equipment across a network
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

# import socket  # SOCKET IS LOADED ON DEMAND

class MsgRouterClient:
    def __init__(self, host="localhost", port=8091):
        self.host = host
        self.port = port
        import socket
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._buffer_size = 2048
        self.socket.connect((self.host, self.port))
        self._methodname = ""

    def _route_request(self, *args):
        msg = "{},{}".format(self._methodname, args[0]).encode('utf-8')
        self.socket.send(msg)
        rsp = self.socket.recv(self._buffer_size)
        if rsp and len(rsp) and rsp != b'_': # "_" character denotes empty responses
            return rsp.decode()

    def __getattr__(self, name):
        self._methodname = name
        return self._route_request

    def close(self):
        self.socket.close()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()
