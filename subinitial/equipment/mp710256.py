#! /usr/bin/env python3
import socket
import time
import logging


logger = logging.getLogger(__name__)


class MP710256:
    MEAS_RETRY_TIMEOUT = 0.02

    def __init__(self, host=None, port=None, autoconnect=True, name="Powersupply"):
        self._host: str = host
        self._port: int = port
        if self._port:
            self._port = int(self._port)
        self._buffer_size = 2048
        self.timeout = 0.8 # seconds
        self._tx_socket: socket.socket = None
        self._rx_socket: socket.socket = None
        self._idn = None
        self._connected = False
        self.name = name

        if host and port and autoconnect:
            self.connect(self._host, self._port)

    def initialize(self):
        '''Put the equipment into a known and ready to work state'''
        self._write("*RST")
        self.set_output(False)
        self.set_current(0)
        self.set_voltage(0)
   
    def connect(self, host=None, port=None):
        update_conn = False

        if host is not None and self._host != host:
            self._host = host
            update_conn = True

        if port is not None and self._port != port:
            self._port = int(port)
            update_conn = True

        if update_conn:
            self.close()
        elif self._connected:
            # No host/port updates, and we're already connected
            return True

        self._tx_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._rx_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._rx_socket.settimeout(self.timeout)
        self._rx_socket.bind(("", self._port))

        idn = self._ask("*IDN?")
        logger.debug("Received IDN: %s", idn)

        if idn is None:
            logger.debug(f"{self.name} unable to connect")
            return False

        if "MP710256" not in idn:
            logger.debug(f"{self.name} unable to connect")
            return False

        logger.debug(f"{self.name} connected")

        self._idn = idn
        self._connected = True

        return True

    def close(self):
        if not self._connected:
            return False

        if self._tx_socket:
            self._tx_socket.close()
            self._tx_socket = None
            
        if self._rx_socket:
            self._rx_socket.close()
            self._rx_socket = None
        
        self._idn = None
        self._connected = False
    
    def _ask(self, msgstr, retries=3):
        for x in range(retries+1):
            self._write(msgstr)
            try:
                response, __ = self._rx_socket.recvfrom(self._buffer_size)
                return response.decode().strip()
            except socket.timeout:
                logger.info(f"Timed out waiting for a reply ({x+1} of {retries})")
                time.sleep(0.2)
        
        raise Exception(f"Command failed after {retries} retries")
    
    def _write(self, msgstr):
        msgstr = msgstr.strip() + "\r"
        self._tx_socket.sendto(msgstr.encode(), (self._host, self._port))

    def _thresh(self, commanded, measured, thresh):
        delta = abs(commanded - measured)
        logger.debug("Commanded: %s, Measured: %s, Delta: %s", commanded, measured, delta) # temporary
        return delta < thresh

    def _wait_for_output(self, commanded, measure_func, thresh, timeout, repeat_delay=0.01):
        timeout = max(timeout, repeat_delay)
        start_time = time.time()

        while True:
            if self._thresh(commanded, measure_func(), thresh):
                # Commanded and measured are within range
                return True

            if time.time() - start_time >= timeout:
                break

            time.sleep(self.MEAS_RETRY_TIMEOUT)

        # Timed out waiting for measured voltage to reach commanded
        return False

    def get_host(self):
        return self._host

    def get_port(self):
        return self._port
 
    def get_connected(self):
        return self._connected
    
    def set_output(self, cmd: bool):
        self._write(f"OUT:{int(cmd)}")

    def enable_output(self):
        # Wait for voltage/current here
        self._write("OUT:1")

    def disable_output(self):
        self._write("OUT:0")

    def get_output(self):
        return bool(int(self._ask("OUT?")))

    def measure_voltage(self):
        return float(self._ask("VOUT?"))

    def get_voltage(self):
        return float(self._ask("VSET?"))

    def set_voltage(self, commanded, timeout=None):
        self._write("VSET:{}".format(commanded))

        if self.get_output() and timeout is not None:
            return self._wait_for_output(commanded, self.get_voltage, 0.01,
                                        timeout, self.MEAS_RETRY_TIMEOUT)

        # Timeout was None or output is off
        return True

    def measure_current(self):
        return float(self._ask("IOUT?"))
    
    def get_current(self):
        return float(self._ask("ISET?"))

    def set_current(self, commanded, timeout=None):
        self._write("ISET:{}".format(commanded))

        if self.get_output() and timeout is not None:
            return self._wait_for_output(commanded, self.get_current, 0.01,
                                        timeout, self.MEAS_RETRY_TIMEOUT)

        # timeout was None or output is off
        return True

    def get_network(self) -> dict:
        return dict(
            ipv4 = self._ask(":SYST:IPAD?"),
            subnet = self._ask(":SYST:SMASK?"),
            gateway = self._ask(":SYST:GATE?"),
            dhcp = self._ask(":SYST:DHCP?"),
            mac = self._ask(":SYST:MAC?"),
            port = self._ask(":SYST:PORT?"),
            baudrate = self._ask(":SYST:BAUD?"),
        )

    NO_CHANGE = None

    def set_network(self, ipv4: str=NO_CHANGE, subnet: str=NO_CHANGE, gateway: str=NO_CHANGE, 
                          dhcp: bool=NO_CHANGE, mac: str=NO_CHANGE, port: int=NO_CHANGE, 
                          baudrate: int=NO_CHANGE) -> dict:
            
            setpoints = {}
            for cmd, title, scpi, convfn in [
                    (ipv4, "ipv4", "IPAD", str),
                    (subnet, "subnet", "SMASK", str),
                    (gateway, "gateway", "GATE", str),
                    (dhcp, "dhcp", "DHCP", lambda x: 1 if bool(int(x)) else 0),
                    (mac, "mac", "MAC", lambda x: str(x).replace(':', '-').lower()),
                    (port, "port", "PORT", int),
                    (baudrate, "baudrate", "BAUD", int),
                ]:
                if cmd != self.NO_CHANGE:
                    cmd = convfn(cmd)
                    setpoints[title] = cmd
                    self._write(f":SYST:{scpi} {cmd}")
                    # readback = self._ask(f":SYST:{scpi}?").strip()
                    # if str(cmd) != readback:
                    #     logger.warning(f"MP710256.%s attemped to program: %s, device reports back: %s", title, cmd, readback)
            return setpoints


def main():
    import argparse
    parser = argparse.ArgumentParser(description='MP710256 Network Configuration Tool')
    parser.add_argument('-i', '--ipv4', help='Present MP710256 ipv4 (192.168.1.198|240)', default="192.168.1.198", required=False)
    parser.add_argument('-p', '--port', help='Present MP710256 UDP port (18190)', default="18190", required=False)
    parser.add_argument('settings', help='e.g. port=18220 ipv4=172.17.40.20 gateway=172.17.0.1', nargs='*')
    args = parser.parse_args()

    # mp = MP710256("192.168.1.198", port=18190)
    # mp = MP710256("172.17.0.127", port=18190)
    mp = MP710256(args.ipv4, port=int(args.port))

    settings = mp.get_network()
    print("Current Network Settings")
    for key, val in settings.items():
        print("\t", key, val)

    if args.settings:
        new_settings = mp.set_network(**{x.split("=")[0]: x.split("=")[1] for x in args.settings})
        print("Setting")
        for key, val in new_settings.items():
            print("\t", key, val)

        settings.update(new_settings)
        mp.close()
        
        input("Power-Cycle the supply then press enter>")
       
        mp = MP710256(settings["ipv4"], port=settings["port"])
        print("Network Settings After Programming")
        settings = mp.get_network()
        for key, val in settings.items():
            print("\t", key, val)
        

if __name__ == "__main__":
    main()
