# -*- coding: UTF-8 -*-
# Subinitial Test Automation Framework rigoldm3000 series interface
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import socket
import logging
import vxi11

logger = logging.getLogger(__name__)


class RigolDG1000Z:
    def __init__(self, host=None, autoconnect=True):
        """Creates an instance to remotely control the test equipment.

        :param host: tcp/ip host as ip-address or Netbios hostname
        :type host: string
        :param autoconnect: if True the device will be connected to at instantiation
        :type autoconnect: bool
        """
        self._host: str = host
        self._port = 5025
        self._timeout = 5
        self._buffer_size = 2048
        self._socket = None
        self.idn = None
        self.connected = False
        if host and autoconnect:
            self.connect(host=self._host)

    def connect(self, host=None):
        """Connect to the device via LAN

        :param host: tcp/ip host as ip-address or Netbios hostname
        :type host: string
        :returns: True if connection was made, False if already connected to host
        :rtype: bool
        """
        if host is not None and self._host != host:
            self.close()
            self._host = host
        elif self.connected:
            return False
        retries = 0
        while True:
            try:
                self.device = vxi11.Instrument(self._host)
                """ THE FOLLOWING TIMEOUT VALUES ARE CRITICAL, THE DMM WILL REPORT THAT IT IS LOCKED IF THESE AREN'T SET
                    The values come from packet sniffing Rigol's Ultra Sigma software while connecting to a DM3068"""
                self.device.lock_timeout = 0
                self.device.timeout = 2000
                self.idn = self._ask("*IDN?")
                break
            except Exception as ex:
                retries += 1
                logger.info("connect failed on try {} with exception: {}".format(retries, ex))
                if retries > 3:
                    raise ex

        self.connected = True
        return True

    def _write(self, msgstr):
        """Send command to the device but don't wait for a response

        :param msgstr: message string
        :type msgstr: str
        """
        self.device.write(msgstr)
        # msgstr = msgstr.strip() + "\r\n"
        # self._socket.send(msgstr.encode())

    def _ask(self, msgstr):
        """Send command to the device and read response

        :param msgstr: message string
        :type msgstr: str
        :return: GPIB response string
        :rtype: str
        """
        return self.device.ask(msgstr)
        # self._write(msgstr)
        # response = self._socket.recv(self._buffer_size).decode()
        # return response.strip()

    def close(self):
        """Close the device connection"""
        if self.connected:
            if self._socket:
                self._socket.close()
            self._socket = None
            self.connected = False

    def __enter__(self):
        return self

    def __exit__(self):
        self.close()

    def is_operation_complete(self):
        """Get operation status of the device

        ..note: this will most likely always return True as the device will not
                respond to it until it is finished completing all of its queued operations
        :return: True if all operations are complete and device is ready for next command
        :rtype: bool
        """
        opc = self._ask("*OPC?")
        return opc != "0"

    def reset(self, wait_for_response=False):
        """Reset the device to its default state"""
        self._write("*RST")
        if wait_for_response:
            return self.is_operation_complete()
        return 0

    # DEVICE SPECIFIC CODE BELOW

    def output(self, state):
        self._write(f":OUTP {1 if state else 0}")

    def get_output(self):
        return self._ask(f":OUTP?").strip() == "ON"

    IMPEDANCE_HIGHZ = "INF"
    IMPEDANCE_50OHM = 50

    def impedance(self, channel=1, isetting=IMPEDANCE_50OHM):
        self._write(f":OUTP{channel}:IMP {isetting}")

    def get_impedance(self, channel=1):
        return float(self._ask(f":OUTP{channel}:IMP?"))

    SHAPE_SINE = "SIN"
    SHAPE_RAMP = "RAMP"
    SHAPE_PULSE = "PULSE"
    SHAPE_NOISE = "NOISE"
    SHAPE_SQUARE = "SQU"

    def waveform(self, channel=1, shape=SHAPE_SQUARE, frequency=10e6, vamplitude=3.3, voffset=3.3/2, phase_offset=0):
        """output a waveform on the specified channel

        :param channel: integer channel, starting from 1.
        :param shape: see self.SHAPE_* options
        :param frequency: wave frequence
        :param vamplitude: peak to peak amplitude in volts
        :param voffset: offset in volts
        :param phase_offset: phase offset in degrees
        """
        self._write(f"SOUR{channel}:APPL:{shape} {frequency},{vamplitude},{voffset},{phase_offset}")

    def get_waveform(self, channel=1):
        """get the waveform setting on the specified channel

        :param channel: integer channel, starting from 1.
        :return: (shape, frequency, vamplitude, voffset, phase_offset)
        """
        elems = self._ask(f"SOUR{channel}:APPL?").strip('"').split(",")
        return elems[0:1] + list(float(x) for x in elems[1:])
