# -*- coding: UTF-8 -*-
# Stacks library utility code
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import sys
import os
import struct
import time
import math


class SubinitialException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


class SubinitialTimeoutError(SubinitialException):
    pass


class SubinitialInputError(SubinitialException):
    pass


class SubinitialResponseError(SubinitialException):
    def __init__(self, msg, tag=None, error_code=0):
        SubinitialException.__init__(self, msg)
        self.tag = tag
        self.error_code = error_code

    def __str__(self):
        return "{}, tag: {}".format(repr(self.msg), repr(self.tag))


class SubinitialResponseErrors(SubinitialException):
    def __init__(self, errors):
        self.errors = errors

    def __str__(self):
        if self.errors is None:
            return "No Errors Found"
        return "\n".join(map(lambda x: repr(x), self.errors))


if sys.version_info >= (3, 0):
    long = int

def clamp(i, imin, imax):
    if i < imin:
        i = imin
    if i > imax:
        i = imax
    return i

def getcreate(adict, key, default):
    val = adict.get(key, None)
    if val is None:
        adict[key] = default
    return default

class open_stubbornly:
    def __init__(self, name, mode='r', buffering=-1, encoding=None, errors=None, newline=None, closefd=True,
                 opener=None, prompt=None):
        if prompt is None:
            prompt = self.default_prompt

        while True:
            try:
                self.f = open(name, mode=mode, buffering=buffering, encoding=encoding, errors=errors, newline=newline,
                         closefd=closefd, opener=opener)
                return
            except FileNotFoundError:
                os.makedirs(os.path.split(name)[0])
            except Exception as ex:
                if not prompt(name):
                    raise ex

    def default_prompt(self, path):
        print("Opening ", path, "failed, try closing any programs that may be accessing it.")
        ui = input("Press Enter to try again, q to quit...")
        if ui is not None and ui.lower().startswith("q"):
            return False
        return True

    def __enter__(self):
        return self.f

    def __exit__(self, type, value, traceback):
        self.f.close()


def do_for(timeout, func, func_args_list=None, startdelay=0, repeatdelay=0.005, sleep=time.sleep):
    """
    :param timeout: float, time in seconds after which
    :param func: callable, function that must return bool, or an iterable with bool as its first element.
    A return value of True indicates the function has completed successfully.
    A return value of False indicates the function must be called again after a delay "cycledelay" so long as "timeout"
    seconds have not yet elapsed since the first call to "func".
    :param func_args_list: list or tuple, arguments that will be passed to func
    :param startdelay: float, optional time in seconds to delay before starting the first call to func
    :param repeatdelay: float, time in seconds to delay before repeating the next call to func
    :return: the last value returned from func before timing out or upon func completing successfully
    """
    func_retval = None
    if startdelay > 0:
        sleep(startdelay)
    start_time = time.monotonic()
    while True:
        if func_args_list is None:
            func_retval = func()
        else:
            func_retval = func(*func_args_list)
        if func_retval is not None:
            if hasattr(func_retval, '__getitem__'):
                if func_retval[0] is True:
                    break
            elif func_retval:
                break
        if time.monotonic() - start_time >= timeout:
            break
        if repeatdelay > 0:
            sleep(repeatdelay)
    return func_retval


def message_to_bytes(*args):
    if len(args) == 0:
        raise SubinitialInputError("message must be bytes, bytearray, string, or variable number integers between 0 and 255")
    elif len(args) == 1:
        if isinstance(args[0], (bytearray, bytes)):
            return args[0]
        elif type(args[0]) == str:
            return args[0].encode()
        elif isinstance(args[0], (tuple, list)):
            return bytearray(*args)
        else:
            return bytearray(args)
            # raise SubinitialInputError("message must be bytes, bytearray, string, or variable number integers between 0 and 255")
    else:
        return bytearray(args)


def mask_to_bits(mask):
    bits = []
    for i in range(0, 32):
        if (1 << i) & mask > 0:
            bits.append(i)
    return bits


def bits_to_csv(title, bitset, default=None, bitlist_string=None):
    csvtext = ""
    if bitlist_string is None:
        for i in range(0, 32):
            if (bitset & (0x1 << i)) > 0:
                    csvtext += str(i+1) + ','
    else:
        for i in range(0, len(bitlist_string)):
            if (bitset & (0x1 << i)) > 0:
                    csvtext += bitlist_string[i] + ','

    if len(csvtext) == 0 and default is not None:
        return default
    else:
        return title + csvtext.strip(',')


def digest_indices(indices, mask, clearmask=0xFFFFFFFF, return_mask_not_list=True, idx2mask=lambda idx:1<<idx):
    if len(indices) > 0:
        if mask > 0:
            raise SubinitialInputError("Cannot use both indices and bitmask as arguments. Pick one.")
        try:
            for index in indices:
                mask |= idx2mask(index)
        except Exception:
            raise SubinitialInputError("Invalid argument. Indices must be integers.")
    mask &= clearmask
    if return_mask_not_list:
        return mask
    else:
        return mask_to_bits(mask)


def round_insignificant_digits(x, n):
    if x == 0:
        return 0
    return round(x, -int(math.floor(math.log10(x)))+(n-1))


def to_eng(x, sigfigs=6, si=True):
    """
    Returns float/int value <x> formatted in a simplified engineering format -
    using an exponent that is a multiple of 3.

    format: printf-style string used to format the value before the exponent.

    si: if true, use SI suffix for exponent, e.g. k instead of e3, n instead of
    e-9 etc.

    E.g. with format='%.2f':
        1.23e-08 => 12.30e-9
             123 => 123.00
          1230.0 => 1.23e3
      -1230000.0 => -1.23e6

    and with si=True:
          1230.0 => 1.23k
      -1230000.0 => -1.23M
    """

    sign = ''
    if x < 0:
        x = -x
        sign = '-'
    x = round_insignificant_digits(x, sigfigs)
    exp = int( math.floor( math.log10( x))) if (x != 0) else 1
    exp3 = exp - ( exp % 3)
    x3 = x / ( 10 ** exp3)

    if si and -24 <= exp3 <= 24 and exp3 != 0:
        exp3_text = 'yzafpnµm kMGTPEZY'[ ( exp3 - (-24)) // 3]
    elif exp3 == 0:
        exp3_text = ''
    else:
        exp3_text = 'E%s' % exp3

    if 1 <= x3 < 10:
        return ('{0}{1:.'+str(sigfigs-1)+'f}{2}').format(sign, x3, exp3_text)
    elif 10 <= x3 < 100:
        return ('{0}{1:.'+str(sigfigs-2)+'f}{2}').format(sign, x3, exp3_text)
    else:
        return ('{0}{1:.'+str(sigfigs-3)+'f}{2}').format(sign, x3, exp3_text)


def adc24_tostr(x, sigfigs=6, min_power=-4):
    if min_power > -4:
        raise SubinitialInputError("Can't use min_power > -4")
    if sigfigs != 6:
        raise SubinitialInputError("sigfigs must be equal to 6")

    if math.pow(10,(min_power + sigfigs)) <= abs(x):
        return to_eng(x, sigfigs)

    sign = ''
    if x < 0:
        sign = '-'
        x = -x

    y = math.pow(10, -min_power)
    x = round(x*y+1e-6)
    xstr = ('{0:0'+str(sigfigs)+'d}').format(x)
    xstr = xstr[:2] + '.' + xstr[2:]

    a = sigfigs + min_power
    exp3 = a - (a % 3)
    exp3_text = 'yzafpnµm kMGTPEZY'[(exp3 - (-24)) // 3]

    return sign + xstr + exp3_text


"""ByteArray, Integer, and Floating Point Conversion Helpers"""
def to_int16(uint16_val):
    if uint16_val >= 0x8000:
        uint16_val = -(0xFFFF - uint16_val + 1)
    return uint16_val

def to_int32(uint32_val):
    if uint32_val >= 0x80000000:
        uint32_val = -(0xFFFFFFFF - uint32_val + 1)
    return uint32_val

def to_uint16(int16_val):
    neg = int16_val < 0
    uval = abs(int16_val)
    if neg:
        uval %= (0xFFFF / 2)
        uval = 0xFFFF - uval + 1
    else:
        uval = min(uval, 0xFFFF)
    return int(uval)

uint32_to_shorts = lambda x: [x & 0xFFFF, (x >> 16) & 0xFFFF]

def to_uint32(int32_val):
    neg = int32_val < 0
    uval = abs(int32_val)
    if neg:
        uval %= (0xFFFFFFFF / 2)
        uval = 0xFFFFFFFF - uval + 1
    else:
        uval = min(uval, 0xFFFFFFFF)
    return int(uval)

def to_float(u32val):
    return struct.unpack('f', struct.pack('L', u32val))[0]

def to_double(u64val):
    return struct.unpack('d', struct.pack('Q', u64val))[0]

def from_float(floatval):
    """
    :param floatval: single precision floating point value
    :return: 32bit unsigned integer
    """
    return struct.unpack('L', struct.pack('f', floatval))[0]

def from_double(doubleval):
    """
    :param doubleval: double precision floating point value
    :return: 64bit unsigned integer
    """
    return struct.unpack('Q', struct.pack('d', doubleval))[0]

def uint32_at(barray, idx):
    return (barray[idx+1] +
            (barray[idx+1] << 8) +
            (barray[idx+1] << 16) +
            (barray[idx+1] << 24))

def int32_at(barray, idx):
    uval = uint32_at(barray, idx)
    return to_int32(uval)

def uint16_at(barray, idx):
    return (barray[idx+1] << 8) + barray[idx]

def int16_at(barray, idx):
    uval = uint16_at(barray, idx)
    return to_int16(uval)

def float_at(barray, idx):
    return struct.unpack_from('L', barray, idx)
"""END ByteArray, Integer, and Floating Point Conversion Helpers"""


class Bitmask(long):
    """
    32-bit Bitmask that acts like an integer with added helper methods.

    .. Note:: Accessing an element of Bitmask at index i returns the bit state at position i.

        e.g. Bitmask(0b10)[1] == True

    .. Note:: Iterating over Bitmask yields bit state at positions 0 through 31.
    """
    def __getitem__(self, index):
        """Access element at index.

        :param int index: bit index
        :return: bit value at specified bit index
        :rtype: bool
        """
        return ((1 << index) & self) > 0

    def __iter__(self):
        for _i in range(0, 32):
            yield _i, self[_i]

    def list_of_setbits(self):
        """Get list of bit indices that are set to 1.

        :return: list of set bit indices, e.g. 0b1010 -> [1, 3]
        :rtype: list(int)
        """
        return list(idx for idx, is_set in self if is_set)

    def extract(self, position, mask):
        """Extract a bitfield out of this 32bit integer bitmask.

        :param position: rightmost bit position of the bitfield
        :param mask: right aligned mask of bitfield length, e.g. 3-bit bitfield mask == 0b111
        :return: Bitmask
        :rtype: int | Bitmask
        """
        return Bitmask((self >> position) & mask)

    def insert(self, position, mask, value):
        """Insert a bitfield into this 32bit integer bitmask.

        :param position: rightmost bit position of the bitfield
        :param mask: right aligned mask of bitfield length, e.g. 3-bit bitfield mask == 0b111
        :param value: bitfield value to be inserted
        :return: Bitmask
        :rtype: int | Bitmask
        """
        t = self & ~(mask << position)
        return Bitmask(t | ((value & mask) << position))

    def setmask(self, mask):
        """Return new Bitmask with mask set.

        :param mask: mask of bits to be set, e.g. mask=0b01 sets bit 0.
        :return: Bitmask
        :rtype: int | Bitmask
        """
        return Bitmask(self | mask)

    def clearmask(self, mask):
        """Return new Bitmask with mask cleared.

        :param mask: mask of bits to be cleared, e.g. mask=0b01 clears bit 0.
        :return: Bitmask
        :rtype: int | Bitmask
        """
        return Bitmask(self & ~mask)

    def to_uint8_array(self):
        return [
            self & 0xFF,
            (self >> 8) & 0xFF,
            (self >> 16) & 0xFF,
            (self >> 24) & 0xFF
        ]

    def to_uint16_array(self):
        return [self & 0xFFFF, (self >> 16) & 0xFFFF]
