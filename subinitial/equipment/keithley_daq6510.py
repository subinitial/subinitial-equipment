'''
Driver for Keithley DAQ6510
'''
import logging

import pyvisa


logger = logging.getLogger(__name__)



class KeithleyDaq6510:
    def __init__(self, host: str):
        self.host = host
        self.inst: pyvisa.resources.TCPIPInstrument|None = None


    ################################################################################
    # Initialization and State Management
    ################################################################################
    def initialize(self):
        '''Put the device in a known and ready to operate state'''
        self.reset()

    def connect(self):
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(f'TCPIP::{self.host}::inst0::INSTR') # type: ignore

    def disconnect(self):
        if self.inst is not None:
            self.inst.close()
            self.inst = None


    ################################################################################
    # I/O
    ################################################################################
    def scpi_write(self, cmd: str):
        assert self.inst is not None

        logger.debug('scpi write: %s', cmd)
        self.inst.write(cmd)

    def scpi_query(self, cmd: str) -> str:
        assert self.inst is not None
        
        logger.debug('scpi write: %s', cmd)
        read = self.inst.query(cmd).strip()
        logger.debug('scpi read: %s', read)
        return read
    
    def flush(self):
        assert self.inst is not None
        while True:
            try:
                self.inst.read()
            except:
                return


    ################################################################################
    # Operations
    ################################################################################
    def reset(self):
        self.scpi_write('*RST')

    def get_idn(self) -> str:
        return self.scpi_query("*IDN?").strip()

    def check_errors(self) -> list[str]:
        errs: list[str] = []
        while (err:=self.scpi_query('SYST:ERR?')) != '0,"No error;0;0 0"':
            errs.append(err)

        return errs