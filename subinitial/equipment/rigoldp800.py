# -*- coding: UTF-8 -*-
# Subinitial Test Automation Framework rigolps800 series interface
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import logging
from .msgrouter_client import MsgRouterClient
from subinitial.equipment.utils import do_for
from subinitial.equipment.exceptions import SubinitialTimeoutError
from .psupply import PsupplyBase
logger = logging.getLogger(__name__)


class RigolDP800(PsupplyBase):
    AUTO = None
    DEFAULT = None
    vid = 0x1AB1
    pid = 0x0E11
    _mode_usb = "USB"
    _mode_dc = "DISCONNECTED"
    _mode_tcpip = "TCPIP"
    _mode_router = "ROUTER"
    _retrycount = 3

    BLOCKING_CYLCEDELAY = 0.02

    def __init__(self, serial=None, host: str = None, name: str = AUTO):
        self.serial = serial
        self.mode = RigolDP800._mode_dc
        self.idn = None
        self.ipaddr = host
        self.device = None
        """:type : usbtmc.Instrument"""
        PsupplyBase.__init__(self, host, name)
        self.default_channel = 1  # 1 is the best default for this rigol!

    def is_connected(self):
        return self.idn is not None

    def connect_usb(self):
        self.mode = RigolDP800._mode_usb
        retries = 0
        import usbtmc
        while True:
            try:
                if self.serial is None:
                    self.device = usbtmc.Instrument(RigolDP800.vid, RigolDP800.pid)
                else:
                    self.device = usbtmc.Instrument(RigolDP800.vid, RigolDP800.pid, iSerial=self.serial)
                break
            except Exception as ex:
                retries += 1
                logger.info("connect failed on try {} with exception: {}".format(retries, ex))
                if retries > RigolDP800._retrycount:
                    raise ex
        self.device.write(" ")
        self.idn = self._get_idn()

    def connect_tcpip(self, ipaddress):
        self.mode = RigolDP800._mode_tcpip
        self.ipaddr = ipaddress
        import vxi11
        retries = 0
        while True:
            try:
                self.device = vxi11.Instrument(self.ipaddr)
                self.idn = self._get_idn()
                break
            except Exception as ex:
                retries += 1
                logger.info("connect failed on try {} with exception: {}".format(retries, ex))
                if retries > RigolDP800._retrycount:
                    raise ex

    def connect_router(self, host="localhost", port=8091):
        self.mode = RigolDP800._mode_router
        self.ipaddr = host
        retries = 0
        while True:
            try:
                self.device = MsgRouterClient(host, port)
                self.idn = self._get_idn()
                break
            except Exception as ex:
                retries += 1
                logger.info("connect failed on try {} with exception: {}".format(retries, ex))
                if retries > RigolDP800._retrycount:
                    raise ex

    def connect(self):
        self.connect_tcpip(self.host)

    def _get_idn(self):
        retries = 0
        while True:
            try:
                return self.device.ask("*IDN?")
            except Exception as ex:
                retries += 1
                logger.info("communication for *IDN? failed on try {} with exception: {}".format(retries, ex))
                if retries > RigolDP800._retrycount:
                    raise ex

    def _check_voltage(self, channel, commanded, threshold):
        meas = self.get_measured(channel)[0]
        delta = abs(meas - commanded)
        return delta < threshold

    def _wait_for_voltage(self, timeout, channel, commanded, threshold):
        threshold = abs(threshold)
        if not do_for(timeout, self._check_voltage,
                      (channel, commanded, threshold),
                      repeatdelay=self.BLOCKING_CYLCEDELAY):
            raise SubinitialTimeoutError("DP800 commanded voltage not reached within {}s".format(timeout))

    def set_channel_nonblocking(self, channel, voltage, current=None):
        """ :param int channel: 1-3
            :param float voltage: 0-30; CH3:0-5;
            :param float current: 0-3; None=No Change """
        if current is None:
            self.device.write(":APPLy CH{},{}".format(channel, voltage))
        else:
            self.device.write(":APPLy CH{},{},{}".format(channel, voltage, current))

    def set_channel(self, channel, voltage, current=None, threshold=0.1, timeout=5.0):
        """Sets channel voltage and current.

        This method blocks until the measured voltage is within threshold volts of the commanded voltage
        if the channel output is enabled. The measured voltage is queried directly from the power supply.

        :param channel: output channel index (between 1-3)
        :type channel: int
        :param voltage: commanded channel voltage in Volts
        :type voltage: float
        :param current: output channel current limit in Amps (no change made if None)
        :type current: None | float
        :param threshold: acceptable difference in measured voltage between commanded voltage in Volts
        :type threshold: float
        :param timeout: maximum time to block while waiting for measured voltage to reach threshold. (0 prevents blocking)
        :type timeout: float
        :raises SubinitialTimeoutError: if *timeout* seconds have elapsed and measured voltage is not within
            *threshold* volts of commanded voltage.
        """
        self.set_channel_nonblocking(channel, voltage, current)
        if timeout and self.get_output(channel):
            self._wait_for_voltage(timeout, channel, voltage, threshold)

    def get_channel(self, channel):
        """ :param int channel: 1-3
            :return float, float: voltage, current """
        response = self.device.ask(":APPLy? CH{}".format(channel)).split(',')
        return float(response[1]), float(response[2])

    def get_vlimit(self, channel=DEFAULT) -> float:
        return self.get_measured(channel or self.default_channel)[0]

    def get_ilimit(self, channel=DEFAULT) -> float:
        return self.get_measured(channel or self.default_channel)[1]

    def set_output(self, state: bool, channel=DEFAULT):
        self.device.write(":OUTP CH{},{}".format(channel or self.default_channel, "ON" if state else "OFF"))

    def get_output(self, channel=DEFAULT) -> bool:
        result = self.device.ask(":OUTP? CH{}".format(channel or self.default_channel))
        return result == "ON"

    def set_vlimit(self, setpoint: float, channel=DEFAULT):
        self.set_channel_nonblocking(channel or self.default_channel, setpoint)

    def set_ilimit(self, setpoint: float, channel=DEFAULT):
        channel = channel or self.default_channel
        self.set_channel_nonblocking(channel, self.get_vlimit(channel), setpoint)
    
    def set_output_and_wait(self, channel, state, threshold=0.1, timeout=5.0):
        """Sets channel output state

        This method blocks until the output voltage measured is within threshold volts of commanded voltage
        if the channel output is commanded to be enabled. Commanded voltage and output voltage are queried directly
        from the power supply.

        :param channel: output channel index (between 1-3)
        :type channel: int
        :param state: commanded output state (True:On, False:Off)
        :type state: bool
       :param threshold: acceptable difference in measured voltage between commanded voltage in Volts
        :type threshold: float
        :param timeout: maximum time to block while waiting for measured voltage to reach threshold. (0 prevents blocking)
        :type timeout: float
        :raises SubinitialTimeoutError: if *timeout* seconds have elapsed and measured voltage is not within
            *threshold* volts of commanded voltage.
        """
        self.set_output_nonblocking(channel, state)
        if timeout and state:
            commanded = self.get_channel(channel)[0]
            self._wait_for_voltage(timeout, channel, commanded, threshold)

    def get_measured(self, channel):
        """ :param int channel: 1 - 3
            :return float, float, float: (voltage, current, power) """
        results = self.device.ask(":MEASure:ALL? CH{}".format(channel)).split(',')
        return float(results[0]), float(results[1]), float(results[2])

    def measure_voltage(self, channel=DEFAULT) -> float:
        return self.get_measured(channel or self.default_channel)[0]

    def measure_current(self, channel=DEFAULT) -> float:
        return self.get_measured(channel or self.default_channel)[1]
    



    def close(self):
        self.idn = None
        if self.device is None:
            return
        try:
            if self.mode == RigolDP800._mode_router or self.mode == RigolDP800._mode_tcpip:
                self.device.close()  # This will close the devices socket
            if self.mode == RigolDP800._mode_usb:
                import usb.util
                self.device.reset()  # TODO: this method does nothing, find a way to reset the SCPI command interface
                usb.util.release_interface(self.device.device, self.device.iface)
        except:
            print("RigolDP800: Unable to close, aborting close.")

    def __enter__(self):
        return self

    def __exit__(self):
        self.close()
