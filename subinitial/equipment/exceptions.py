# -*- coding: UTF-8 -*-
# Stacks internal library
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.


class SubinitialException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


class SubinitialTimeoutError(SubinitialException):
    pass


class SubinitialInputError(SubinitialException):
    pass


class SubinitialResponseError(SubinitialException):
    def __init__(self, msg, tag=None, error_code=0):
        SubinitialException.__init__(self, msg)
        self.tag = tag
        self.error_code = error_code

    def __str__(self):
        return "{}, tag: {}".format(repr(self.msg), repr(self.tag))


class SubinitialResponseErrors(SubinitialException):
    def __init__(self, errors):
        self.errors = errors

    def __str__(self):
        if self.errors is None:
            return "No Errors Found"
        return "\n".join(map(lambda x: repr(x), self.errors))
