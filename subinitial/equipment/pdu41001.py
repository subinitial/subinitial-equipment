import socket
import time
import logging

logger = logging.getLogger(__name__)


class Pdu41001:
    """
    CyberPower LAN PDU
    """

    def __init__(self, host: str|None=None, username="cyber", password="cyber", autoconnect=True):
        self.name = "PDU41001"
        self._host = host if host is not None else "172.17.31.13"
        self._port = 23
        self._timeout = 5
        self._buffer_size = 2048
        self._socket = None
        # self._tel: Telnet = None
        self.username = username
        self.password = password
        self.connected = False
        if autoconnect and host:
            self.connect(host=self._host)
        
    
    def connect(self, host=None):
        """Connect to the device via LAN

        :param host: tcp/ip host as ip-address or Netbios hostname
        :type host: string
        :returns: True if connection was made, False if already connected to host
        :rtype: bool
        """

        if host is not None and self._host != host:
            self.close()
            self._host = host
        elif self.connected:
            return False

        # logger.debug("New TelNet connection to: %s:%s @ timeout=%s", self._host, self._port, self._timeout)
        # self._tel = Telnet(self._host, self._port, self._timeout)
        # self._tel.open(self._host)
        # self._tel.get_socket().sendall(b"\xff\xfd\x03" + b"\xff\xfb\x18" + b"\xff\xfb\x1f" + b"\xff\xfb\x20" + b"\xff\xfb\x21" + b"\xff\xfb\x22" + b"\xff\xfb\x27" + b"\xff\xfd\x05")        
        # self._tel.get_socket().sendall( b"\xff\xfd\x05")        
        # logger.debug("waiting for login prompt")
        # x = self._tel.read_until(b'ame:', 2)
        # logger.debug("got: %s", x)
        # logger.debug("sending username: %s", self.username)
        # self._tel.write(self.username.encode() + b'\r')
        # x = self._tel.read_until(b'assword:', 2)
        # logger.debug("got: %s", x)
        # self._tel.write(self.password.encode() + b'\r')

        # logger.debug("waiting for login...")
        # x = self._tel.read_until(b"CyberPower >", 2)
        # logger.debug("got: %s", x)
        # logger.debug("command-line connected!")

        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.settimeout(self._timeout)
        self._socket.connect((self._host, self._port))
        
        self._socket.sendall(b"\xff\xfd\x05")
        motd = self._ask(None)
        prompt = self._ask(self.username)
        self._write(self.password)
        time.sleep(1.5)
        cmdline = self._ask(None)
        

        # self.idn = self._ask("*IDN?")
        self.connected = True
        # if device_id != self.ID:
        #     raise Exception("Invalid Device ID? response: {}, expected {}".format(device_id, self.ID))
        
        return True

    def get_status(self) -> list[bool]:
        """
        Get all PDU channel on/off status
        """
        resp = self._ask("oltsta show")
        chans = [None] * 9
        for line in resp.splitlines():
            # print("**", line)
            if "Outlet" not in line:
                continue
            chan, name, stat = line.strip().lower().split()
            chans[int(chan)] = stat == "on"
        return chans

    def reboot(self, channel: int, delay=True):
        """
        reboots a PDU channel
        """
        resp = self._ask(f"oltctrl index {channel} act {'delayreboot' if delay else 'reboot'}")

    def cmd(self, channel: int, state: bool):
        """
        commands a PDU channel on or off
        """
        resp = self._ask(f"oltctrl index {channel} act {'on' if state else 'off'}")


    def _write(self, msgstr: str):
        """Send command to the device but don't wait for a response

        :param msgstr: message string
        :type msgstr: str
        """
        msgstr = msgstr.strip() + "\r"
        txd = msgstr.encode()
        logger.debug("telnet.send(%s)", txd)
        self._socket.send(txd)

    def _ask(self, msgstr):
        """Send command to the device and read response

        :param msgstr: message string
        :type msgstr: str
        :return: GPIB response string
        :rtype: str
        """
        if msgstr:
            self._write(msgstr)
        response = self._socket.recv(self._buffer_size)
        # logger.debug("telnet.recv() -> %s", response)
        try:
            return response.decode().strip()
        except:
            return response
        # return response.strip()


    def close(self):
        """Close the device connection"""
        if self.connected:
            if self._socket:
                self._ask("exit")
                self._socket.close()
            # if self._tel:
            #     self._tel.write(b'exit\n')
            #     self._tel.close()
            self._socket = None
            self._tel = None
            self.connected = False

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()


if __name__ == "__main__":
    # logging.basicConfig(level=logging.DEBUG)
    pdu = PDU41001(username="subinitial", password="subinitial")
    pdu.connect("172.17.31.13")

    try:
        while True:
            cmd = input(">").strip()
            if cmd == "1":
                print(pdu.get_status())
            if cmd == "2":
                pdu.reboot(6)
            if cmd == "3":
                pdu.cmd(6, False)
                # cmd = "oltctrl index 6 act off"
            if cmd == "4":
                # cmd = "oltctrl index 6 act on"
                pdu.cmd(6, True)
            
            # print(rsp)

    except KeyboardInterrupt:
        pass
    pdu.close()
