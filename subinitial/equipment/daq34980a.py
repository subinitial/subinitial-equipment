# -*- coding: UTF-8 -*-
# Subinitial Equipment Library Keysight 34980A series interface
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import time
from datetime import datetime
import logging
from enum import Enum, IntFlag
from dataclasses import dataclass
from typing import Counter, Dict, Iterable, Tuple, Union, List
import pyvisa

logger = logging.getLogger(__name__)


class Daq34980A:
    # NOTE - the parsing code assumes these formatting values
    _RAW_VALUES_PER_CHANNEL = 9    # NOTE - might be 10, documentation has conflicting info...
    _FORMAT_ALARM = 'ON'
    _FORMAT_CHANNEL = 'ON'
    _FORMAT_TIME = 'ON'
    _FORMAT_TIME_TYPE = 'ABSolute'
    _FORMAT_UNIT = 'ON'

    def __init__(self, host=None):
        self.host = host
        self.inst: pyvisa.resources.tcpip.TCPIPInstrument = None    # Change hint to MessageBasedResource?
        self.scan_channels = []

    def connect(self):
        """
        Connect PyVISA to the device
        """
        # Connect to DAQ with PyVISA
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(f'TCPIP::{self.host}::INSTR')

    def disconnect(self):
        """
        Disconnect PyVisa from the device
        """
        if self.inst:
            self.inst.close()
            self.inst = None

    def reset(self):
        """
        Clear volatile memory
        """
        self.scpi_write('*RST')

    def get_idn(self) -> str:
        return self.scpi_query("*IDN?").strip()
    
    def set_format(self):
        """
        Sets the format which will always be used in this driver
        """
        self.scpi_write(f"FORMat:READing:ALARm {self._FORMAT_ALARM}")
        self.scpi_write(f"FORMat:READing:CHANnel {self._FORMAT_CHANNEL}")
        self.scpi_write(f"FORMat:READing:TIME {self._FORMAT_TIME}")
        self.scpi_write(f"FORMat:READing:TIME:TYPE {self._FORMAT_TIME_TYPE}")
        self.scpi_write(f"FORMat:READing:UNIT {self._FORMAT_UNIT}")
    
    def scpi_write(self, command: str):
        logger.debug('writing to DAQ: %s', command)
        self.inst.write(command)

    def scpi_query(self, command: str) -> str:
        logger.debug('writing to DAQ: %s', command)
        reply = self.inst.query(command)
        logger.debug('read from DAQ: %s', reply)
        return reply
    
    class ChannelReading(float):
        class Units(Enum):
            Ohms = "OHM"
            Volts = "VDC"
            VoltsAC = "VAC"
            Amps = "AMP"
            
        def __new__(cls, meas_units: List[str], *args):
            return float.__new__(cls, float(meas_units[0]))
        
        def __init__(self, meas_units: List[str], strrep: List[str]):
            year = int(strrep[1])
            month = int(strrep[2])
            day = int(strrep[3])
            hour = int(strrep[4])
            minute = int(strrep[5])
            second = int(float(strrep[6]))
            microsecond = int(int(strrep[6].split('.')[1]) * 1000)  # NOTE - going from miliseconds to microseconds
            datetime_stamp = datetime(year, month, day, hour, minute, second, microsecond)

            self.units = self.Units(meas_units[-1])
            self.datetime_stamp: datetime = datetime_stamp
            self.channel: int = int(strrep[7])
            self.alarm: int = int(strrep[8])

        def __repr__(self):
            return f'ChannelReading(CH{self.channel}: {self.__float__(): 9.6f} {self.units.name} @{str(self.datetime_stamp)} Alarm:{self.alarm})'
    
    def _fetch(self) -> Dict[int, ChannelReading]:
        # get the big string
        resp = self.scpi_query('FETCh?')
        data = resp.split(',')
        
        # group the data into channels
        channel_count = int(len(data) / Daq34980A._RAW_VALUES_PER_CHANNEL)

        channel_readings: Dict[int, Daq34980A.ChannelReading] = {}
        for i in range(channel_count):
            start_index = i * Daq34980A._RAW_VALUES_PER_CHANNEL
            end_index = start_index + Daq34980A._RAW_VALUES_PER_CHANNEL
            raw_channel_data = data[start_index:end_index]

            reading = self.ChannelReading(raw_channel_data[0].split(" "), raw_channel_data)        
            channel_readings[reading.channel] = reading
        
        return channel_readings

    def scan_route(self, *channels: int):
        self.scpi_write(f"ROUTe:SCAN {self._ichannels_to_scpi(channels)}")

    def scan_initiate(self):
        self.scpi_write("INITiate")

    def scan_fetch(self, timeout=10) -> Dict[int, ChannelReading]:
        t0 = time.time()
        while self.get_measurement_status():
            if time.time() - t0 > timeout:
                raise TimeoutError(f"{self.__class__.__name__}: Unable to fetch data. Scan remained busy after the specified timeout of {timeout} seconds.")
            time.sleep(0.01)
        return self._fetch()

    def scan_measure(self, timeout=10) -> Dict[int, ChannelReading]:
        self.scan_initiate()
        return self.scan_fetch(timeout=timeout)

    def get_errors(self) -> List[Tuple[int, str]]:
        """
        Returns all errors in the device queue (20 max)
        result is in the form of a list of tuples, where the tuples contain (code, text)
        """
        NO_ERROR_CODE = 0
        OVERFLOW_CODE = -350

        def _parse_error(strrep) -> Tuple[int, str]:
            code, text = strrep.split(',')
            code = int(code)
            text = text[1:-2]

            return (code, text)

        errors = []
        for i in range(100):
            error = _parse_error(self.scpi_query("SYSTem:ERRor?"))
            if error[0] == NO_ERROR_CODE:
                break
            errors.append(error)
            if error[0] == OVERFLOW_CODE: # still break, but allow it to be appended to the list first
                break
            
        return errors

    @staticmethod
    def _ichannels_to_scpi(ichannels: Union[int, Iterable[int]]):
        if type(ichannels) == int:
            ichannels = (ichannels,)
        return f"(@{','.join(str(c) for c in ichannels)})"

    class Config:
        AUTO = "AUTO"
        MIN = "MIN"
        MAX = "MAX"
        DEFAULT = "DEF"

        @staticmethod
        def calc_res_range(max_expected: Union[int, float]):
            """ Returns a reasonable range for a resistance reading, to avoid slowdowns with the built-in AUTO range """
            if max_expected < 5e1:
                return '100'
            elif max_expected < 5e2:
                return '1000'#'1k'
            elif max_expected < 5e3:
                return '10000'#'10k'
            elif max_expected < 5e4:
                return '100000'#'100k'
            elif max_expected < 5e5:
                return '1000000'#'1M'
            else:
                return '10000000'#'10M'

        @staticmethod
        def calc_volt_range(max_expected: Union[int, float]):
            """ Sets a reasonable range for a voltage reading, to avoid slowdowns with the built-in AUTO range """
            if max_expected < 5e-2:
                return '0.1'#'100m'
            elif max_expected < 5e-1:
                return '1'
            elif max_expected < 5e0:
                return '10'
            elif max_expected < 5e1:
                return '100'
            else:
                return '300'
    
        class Type(Enum):
            # Counter = "COUNter"
            CurrentDC = "CURRent:DC"
            CurrentAC = "CURRren:AC"
            # Digital = "DIGital"
            # Frequency = "FREQuency"
            FourWireResistance = "FRESistance"
            Resistance = "RESistance"
            # Temperature = "TEMPerature"  
            VoltageDC = "VOLTage:DC"
            VoltageAC = "VOLTage:AC"

        class AutoZero(Enum):
            OFF = "OFF"
            ONCE = "ONCE"
            ON = "ON"
            DEFAULT = "DEFAULT"

        autozero_default = AutoZero.ON

        @classmethod
        def set_autozero_default(cls, setting: AutoZero):
            """Set the default channel auto-zero setting to use when autozero is omitted from Config object"""
            cls.autozero_default = setting

        @classmethod
        def voltage_dc(cls, range: float=AUTO, autozero=AutoZero.DEFAULT, nplc=DEFAULT, resolution=DEFAULT):
            """
            @param range: (100e-3, 1, 10, 100, 300) Volts or "AUTO", "MIN", "MAX"
            @param nplc: number of power-line cycles (use in place of resolution)
            @param resolution: default is 0.000003*range (Volts)
                               NOTE: Only specify resolution when range!=AUTO, and nplc==DEFAULT
            """
            return cls(cls.Type.VoltageDC, range, autozero, nplc, resolution)

        @classmethod
        def voltage_ac(cls, range: float=AUTO, autozero=AutoZero.DEFAULT):
            """
            @param range: (100e-3, 1, 10, 100, 300) Volts or "AUTO", "MIN", "MAX"
            NOTE: the API has a resolution parameter, but it appears to do nothing
            """
            return cls(cls.Type.VoltageAC, range, autozero)

        @classmethod
        def current_dc(cls, range: float=AUTO, autozero: AutoZero=AutoZero.DEFAULT, nplc=DEFAULT, resolution=DEFAULT):
            """
            @param range: (10 mA, 100 mA, 1 A) or "AUTO", "MIN", "MAX"
            @param nplc: number of power-line cycles (use in place of resolution)
            @param resolution: default is 0.000003*range (Amps)
                               NOTE: Only specify resolution when range!=AUTO, and nplc==DEFAULT
            """
            return cls(cls.Type.CurrentDC, range, autozero, nplc, resolution)

        @classmethod
        def current_ac(cls, range: float=AUTO, autozero: AutoZero=AutoZero.DEFAULT):
            """
            @param range: (10 mA, 100 mA, 1 A) or "AUTO", "MIN", "MAX"
            NOTE: the API has a resolution parameter, but it appears to do nothing
            """
            return cls(cls.Type.CurrentAC, range, autozero)

        @classmethod
        def resistance(cls, range: float=AUTO, autozero: AutoZero=AutoZero.DEFAULT, nplc=DEFAULT, resolution=AUTO):
            """
            @param range: (100 ohms, 1 kohms, 10 kohms, 100 kohms, 1 Mohms, 10 Mohms, 100 Mohms) or "AUTO", "MIN", "MAX"
            @param nplc: number of power-line cycles (use in place of resolution)
            @param resolution: default is 0.000003*range (ohms)
                               NOTE: Only specify resolution when range!=AUTO, and nplc==DEFAULT
            """
            return cls(cls.Type.Resistance, range, autozero, nplc, resolution)

        @classmethod
        def four_wire_resistance(cls, range: float=AUTO, autozero: AutoZero=AutoZero.DEFAULT, nplc=AUTO, resolution=AUTO):
            """
            @param range: (100 ohms, 1 kohms, 10 kohms, 100 kohms, 1 Mohms, 10 Mohms, 100 Mohms) or "AUTO", "MIN", "MAX"
            @param nplc: number of power-line cycles (use in place of resolution)
            @param resolution: default is 0.000003*range (ohms)
                               NOTE: Only specify resolution when range!=AUTO, and nplc==DEFAULT
            """
            return cls(cls.Type.FourWireResistance, range, autozero, nplc, resolution)

        def __init__(self, type: Type=None, range: Union[int, str]=AUTO, autozero=AutoZero.DEFAULT, nplc=AUTO, resolution=AUTO):
            self.type = type
            self.range = range
            self.autozero: self.AutoZero = autozero
            self.nplc = nplc
            self.resolution = resolution

        def _generate_scpi(self, ichannels) -> List[str]:
            chanstr = Daq34980A._ichannels_to_scpi(ichannels)
            zero = self.autozero.value if self.autozero != self.AutoZero.DEFAULT else self.autozero_default.value
            resolution = ""
            if self.nplc == self.AUTO and self.resolution != self.AUTO:
                resolution = f"{self.resolution},"
            scpi_cmds = [
                f"CONFigure:{self.type.value} {self.range},{resolution}{chanstr}"
            ]
            if self.nplc != self.AUTO:
                if resolution != "":
                    logger.error("Invalid channel config. Nplc and resolution cannot both be specified: %s", self)
                else:
                    scpi_cmds.append(f"SENSe:{self.type.value}:NPLC {self.nplc},{chanstr}")
            scpi_cmds.append(f"SENSe:{self.type.value}:ZERO:AUTO {zero},{chanstr}")
            return scpi_cmds

        def __repr__(self) -> str:
            return f"{self.type.value}(range:{self.range},zero:{self.autozero.value},nplc:{self.nplc},res:{self.resolution})"

    def configure_channels(self, channelsets: Dict[Config, Union[int, Iterable[int]]], auto_route_channels=True):
        """
        @param channelsets: A dict with the keys Config objects (see above), and the values being a string containing channel selections
        examples of channel selection:
            1001:1070
            1001:1005, 1008
            10003, 1005, 1009
            etc...
        code example:
            daq.configure_channels({daq.Config.resistance(range=10): 1003})
        """
        for channel_config, channels in channelsets.items():
            for scpistr in channel_config._generate_scpi(channels):
                self.scpi_write(scpistr)
        self.set_format()
        
        if auto_route_channels:
            all_channels = []
            for ichannels in channelsets.values():
                if type(ichannels) == int:
                    ichannels = (ichannels,)
                all_channels += list(ichannels)
            self.scan_route(*all_channels)


    class Condition(IntFlag):
        NONE = 0
        CALIBRATION_IN_PROGRESS = 1
        MEASURING = 16
        WAITING_FOR_TRIGGER = 32
        CONFIGURATION_CHANGE = 256
        MEMORY_THRESHOLD = 512
        INSTRUMENT_LOCKED = 1024
        SEQUENCE_READING = 16384

    def get_condition(self) -> Condition:
        """
        Returns the value of the condition register of the DAQ
        """
        return Daq34980A.Condition(int(self.scpi_query('STATus:OPERation:CONDition?')))

    def get_measurement_status(self) -> bool:
        """
        Check the "measurement in progress" flag on the DAQ
        """
        return Daq34980A.Condition.MEASURING in self.get_condition()
