# Subinitial Equipment Library

For more information visit this library's wiki at:
https://bitbucket.org/subinitial/subinitial-equipment/wiki/Home

Subinitial Stacks information:
https://subinitial.com

Git repository:
https://bitbucket.org/subinitial/subinitial-equipment.git


## Introduction

The Subinitial Equipment Library provides interface code for a wide variety of
COTS equipment.


## Requirements
- Python 3.10+
- **python** and **pip3** on your system PATH. 


Use the commands below in a command prompt to verify your installed versions:

```
python --version
python3 --version
pip3 --version
```

## Optional Requirements
- Kvaser USB CAN Support:
  - To use the Kvaser functionality of the CAN part of this package, the appropriate Kvaser CAN driver for your system must be installed. Such drivers can be found on Kvaser's download page (https://www.kvaser.com/download/).
	- `pip3 install "subinitial-equipment[can] @ git+https://bitbucket.org/subinitial/subinitial-equipment.git"`
- DAQ pyvisa support:
  - `pip3 install "subinitial-equipment[daq] @ git+https://bitbucket.org/subinitial/subinitial-equipment.git"`
- All Optional Features
  - `pip3 install "subinitial-equipment[can,daq] @ git+https://bitbucket.org/subinitial/subinitial-equipment.git"`

## Installation

Install all requirements, then open a command prompt and run the **pip3** command below
```
pip3 install git+https://bitbucket.org/subinitial/subinitial-equipment.git
```
NOTES:

- `pip` should work as well as `pip3` if Python 2 is not installed on your system
- pip's `--user` flag allows you to install this package without admin privileges,
  If you'd like to install the package system-wide then omit the `--user` flag e.g. `pip3 install git+https://bitbucket.org/`...
- You can use virtualenv to make a localized Python environment for a particular project then
  pip install all required packages as needed with the virtualenv activated. Omit the `--user`
  flag when installing this package inside a virtualenv.
- You can distribute a specific version of the library with your Python code easily 
  by using the command below. The command below creates the **subinitial-equipment** package from the 
  git tag **v1.13.0** in the directory "**.**" which is the current working directory (CWD)
```
pip3 install git+https://bitbucket.org/subinitial/subinitial-equipment.git@v1.13.0 --target="."
```


## Verify Installation
```
#!python
from subinitial.equipment.tdkzplus import TdkZPlus
print(TdkZPlus)
```

For more detailed installation instructions and help getting started refer to the [Getting Started](https://bitbucket.org/subinitial/subinitial/wiki/Getting_Started) page.


## Included Third-Party Libraries

Third-Party libraries have been included for convenience in the package subdirectory:
    subinitial/equipment/lib

### LICENSE AND COPYRIGHT

Each 3rd party python package has been included with subinitial.equipment for automatic fall-back usage when these packages are not found in your existing python library. All are optional or situationally dependent. Included are web-links for each package, please refer to these websites for copyrights, licensing, and author information.

serial - https://pypi.python.org/pypi/pyserial/2.7
usb - https://pypi.python.org/pypi/pyusb/1.0.0b2
usbtmc - https://pypi.python.org/pypi/python-usbtmc/0.5
vxi11 - https://pypi.python.org/pypi/python-vxi11/0.7

### Optional USB Installation

PyUSB is required to connect to equipment over USB. We recommend you use TCP/IP
when available for improved reliability. If you choose to use USB interfaces then
here are some guidelines:

usb -- PyUSB Install

#### Example for Debian Based GNU/Linux:
	Install libusb-1.0-0 with the following command:
		sudo apt-get install libusb-1.0-0

	For issues refer to:
		https://github.com/walac/pyusb

	You must add your device to the usbtmc rules file to give permission to connect:
		/etc/udev/rules.d/usbtmc.rules

		# USBTMC instruments
		# Agilent DSO-X 2004A
		SUBSYSTEMS=="usb", ACTION=="add", ATTRS{idVendor}=="0957", ATTRS{idProduct}=="179a", GROUP="usbtmc", MODE="0660"

	If you don't want to change the usbtmc rules you can run your python code with administrative priviledges with the "sudo" command refer to:
		http://dontbuyjustmake.blogspot.com/2013/12/controlling-oscilloscope-with-linux.html
